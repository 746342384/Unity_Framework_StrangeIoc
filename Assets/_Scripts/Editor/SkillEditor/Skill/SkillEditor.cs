using UnityEditor;
using UnityEngine;

namespace _Scripts.Editor.SkillEditor.Skill
{
    public class SkillEditor : EditorWindow
    {
        [MenuItem("Skills/SkillEditor")]
        private static void ShowWindow()
        {
            var window = GetWindow<SkillEditor>();
            window.titleContent = new GUIContent("SkillEditor");
            window.Show();
        }

        private void OnGUI()
        {
            
        }
    }
}