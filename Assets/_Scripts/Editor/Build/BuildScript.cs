using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public static class BuildScript
{
    [MenuItem("Build/Perform Build")]
    public static void PerformBuild()
    {
        var projectName = PlayerSettings.productName; // 获取项目名称
        var version = PlayerSettings.bundleVersion; // 获取版本号
        var buildPath = $"RemoteResPackage/Build/{BuildTarget.StandaloneWindows64}/{projectName}_{version}.exe"; // 设置构建输出目录和可执行文件名
        var buildTarget = BuildTarget.StandaloneWindows64; // 设置构建目标平台

        Build(buildPath, buildTarget);
    }

    private static void Build(string buildPath, BuildTarget buildTarget)
    {
        Debug.Log("开始构建项目...");

        // 获取场景列表
        var scenes = GetScenesInBuild();

        // 设置构建选项
        var buildOptions = BuildOptions.None;

        // 开始构建
        var buildReport = BuildPipeline.BuildPlayer(scenes, buildPath, buildTarget, buildOptions);

        if (buildReport.summary.result == BuildResult.Succeeded)
        {
            Debug.Log("构建成功！");
        }
        else
        {
            Debug.LogError("构建失败。");
        }
    }

    private static string[] GetScenesInBuild()
    {
        var scenes = new List<string>();
        foreach (var scene in EditorBuildSettings.scenes)
        {
            if (scene.enabled)
            {
                scenes.Add(scene.path);
            }
        }

        return scenes.ToArray();
    }
}