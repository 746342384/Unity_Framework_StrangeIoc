using System.Diagnostics;
using UnityEditor;

namespace _Scripts.Editor
{
    public static class BuildLuaScript
    {
        //python.exe文件绝对路劲
        private const string PythonExePath = "C:/Users/Administrator/AppData/Local/Programs/Python/Python310/python.exe";
        //python打包脚本绝对路劲
        private const string FilePath = "e:/Project/StrangeIoc/Assets/_Scripts/Editor/Build/Build.py";

        [MenuItem("Build/Lua Build")]
        public static void Build()
        {
            var process = new Process();
            process.StartInfo.FileName = @"powershell.exe";
            process.StartInfo.Arguments = $"& {PythonExePath} {FilePath}";
            process.Start();
        }
    }
}