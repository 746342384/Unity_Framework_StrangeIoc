import os
import getpass
import shutil
import hashlib
import json
import time

# 配置你的路径
lua_scripts_path = "E:\\Project\\StrangeIoc\\LuaScripts"
binary_folder_path = "E:\\Project\\StrangeIoc\\RemoteResPackage\\Lua"
record_json_path = "E:\\Project\\StrangeIoc\\RemoteResPackage\\Lua\\metadata.json"

# 保存文件的哈希值
hashes = {}
# 使用当前的 Unix 时间戳作为版本号
version = int(time.time())

# 遍历LuaScripts目录下的每一个文件
for dir_name in os.listdir(lua_scripts_path):
    dir_path = os.path.join(lua_scripts_path, dir_name)

    if os.path.isdir(dir_path):
        # 读取这个目录中的所有.lua文件
        for root, dirs, files in os.walk(dir_path):
            for file_name in files:
                if file_name.endswith('.lua'):
                    lua_file_path = os.path.join(root, file_name)
                    lua_file_target_path = os.path.join(binary_folder_path, dir_name, file_name)

                    # 创建目标文件夹
                    os.makedirs(os.path.dirname(lua_file_target_path), exist_ok=True)

                    # 拷贝 Lua 文件到目标文件夹
                    shutil.copy2(lua_file_path, lua_file_target_path)

                    print('Copied to ' + lua_file_target_path)

                    # 计算文件的哈希值
                    with open(lua_file_target_path, 'rb') as file:
                        relative_path = os.path.relpath(lua_file_target_path, binary_folder_path)
                        hashes[relative_path] = hashlib.sha256(file.read()).hexdigest()

# 保存哈希值和版本号到 json 文件
metadata = {'version': version, 'hashes': hashes}
with open(record_json_path, 'w') as file:
    json.dump(metadata, file)
