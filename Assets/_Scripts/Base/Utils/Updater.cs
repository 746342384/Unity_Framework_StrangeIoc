using System;
using UnityEngine;

namespace Dxx.Util
{
	public class Updater : MonoBehaviour
	{
		public event Action<float> onUpdate;
		public event Action<float> onUpdateIgnoreTime;
		public event Action onLateUpdate;
		public event Action onFixedUpdate;
		public event Action<float> onUpdateUI;
		public event Action<float> onUpdateUIIgnoreTime;

		private void Update()
		{
			_deltatime = Time.deltaTime;
			_AliveTime += _deltatime;
			if (!GameLogic.Paused)
			{
				_unscaledeltatime = Time.unscaledDeltaTime;
				_unscaleAliveTime += _unscaledeltatime;
			}

			onUpdateIgnoreTime?.Invoke(deltaIgnoreTime);
			if (onUpdate != null && !GameLogic.Paused)
			{
				onUpdate(delta);
			}
			if (onUpdateUI != null && !GameLogic.Paused)
			{
				onUpdateUI(delta);
			}
		}

		public void OnRelease()
		{
			count = 0;
		}

		private void LateUpdate()
		{
			if (onLateUpdate != null && !GameLogic.Paused)
			{
				onLateUpdate();
			}
		}

		private void FixedUpdate()
		{
			if (onFixedUpdate != null && !GameLogic.Paused)
			{
				onFixedUpdate();
			}
		}

		public static Updater Get(GameObject go)
		{
			return go.GetComponent<Updater>() ?? go.AddComponent<Updater>();
		}

		public static Updater GetUpdater()
		{
			if (update == null)
			{
				var gameObject = new GameObject("updater");
				DontDestroyOnLoad(gameObject);
				update = Get(gameObject);
				bCreate = true;
			}
			return update;
		}

		public static void UpdaterDeinit()
		{
			if (update != null)
			{
				Destroy(update.gameObject);
				update = null;
				bCreate = false;
			}
		}

		public static void AddUpdate(string name, Action<float> func, bool IgnoreTimeScale = false)
		{
			var updater = GetUpdater();
			if (!IgnoreTimeScale)
			{
				updater.onUpdate += func;
			}
			else
			{
				updater.onUpdateIgnoreTime += func;
			}
		}

		public static void RemoveUpdate(string name, Action<float> func)
		{
			if (bCreate)
			{
				var updater = GetUpdater();
				updater.onUpdate -= func;
				updater.onUpdateIgnoreTime -= func;
			}
		}

		public static void AddLateUpdate(Action func)
		{
			GetUpdater().onLateUpdate += func;
		}

		public static void RemoveLateUpdate(Action func)
		{
			if (bCreate)
			{
				GetUpdater().onLateUpdate -= func;
			}
		}

		public static void AddFixedUpdate(Action func)
		{
			GetUpdater().onFixedUpdate += func;
		}

		public static void RemoveFixedUpdate(Action func)
		{
			if (bCreate)
			{
				GetUpdater().onFixedUpdate -= func;
			}
		}

		public static void AddUpdateUI(Action<float> func, bool IgnoreTimeScale = false)
		{
			if (!IgnoreTimeScale)
			{
				GetUpdater().onUpdateUI += func;
			}
			else
			{
				GetUpdater().onUpdateUIIgnoreTime += func;
			}
		}

		public static void RemoveUpdateUI(Action<float> func)
		{
			if (bCreate)
			{
				GetUpdater().onUpdateUI -= func;
				GetUpdater().onUpdateUIIgnoreTime -= func;
			}
		}

		public static float AliveTime
		{
			get
			{
				return Time.time;
			}
		}

		public static float delta => Time.deltaTime;

		public static float deltaIgnoreTime => Time.unscaledDeltaTime;

		public static float unscaleAliveTime => Time.unscaledTime;

		private static bool bCreate;

		private static float _AliveTime;

		private static float _deltatime;

		private static float _unscaleAliveTime;

		private static float _unscaledeltatime;

		public int count;

		private static Updater update;
	}
}
