namespace Base.UI
{
    public static class PanelPath
    {
        public const string LoginPanel = "Assets/ResPackage/Common/UIPanel/LoginPanel.prefab";
        public const string MainPanel = "Assets/ResPackage/Common/UIPanel/MainPanel.prefab";
        public const string GamePanel = "Assets/ResPackage/Common/UIPanel/GamePanel.prefab";
        public const string UpdatePanel = "Assets/ResPackage/Common/UIPanel/UpdatePanel.prefab";
        public const string StartPanel = "Assets/ResPackage/Common/UIPanel/StartPanel.prefab";
    }
}