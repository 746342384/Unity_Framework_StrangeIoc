namespace Base.UI
{
    public static class PanelNames
    {
        public const string LoginPanel = nameof(LoginPanel);
        public const string MainPanel = nameof(MainPanel);
        public const string GamePanel = nameof(GamePanel);
        public const string UpdatePanel = nameof(UpdatePanel);
        public const string StartPanel = nameof(StartPanel);
    }
}