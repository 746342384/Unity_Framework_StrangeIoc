using framework.framework.ui.api;
using XLua;

public static class UIManager
{
    [LuaCallCSharp]
    public static void OpenPanel(string panelName)
    {
        var panelSystem = GameContext.Instance.GetService<IPanelSystem>();
        panelSystem.OpenPanel(panelName);
    }

    [LuaCallCSharp]
    public static void ClosePanel(string panelName)
    {
        GameContext.Instance.GetService<IPanelSystem>().ClosePanel(panelName);
    }
}