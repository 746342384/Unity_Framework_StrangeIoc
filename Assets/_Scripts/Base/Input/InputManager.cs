using _Scripts.Framework.framework.input.impl;
using UnityEngine;

namespace _Scripts.Base.Input
{
    public class InputManager : MonoBehaviour
    {
        public static InputManager Ins { get; private set; }
        private IInput input;
        private bool esc = true;

        private void Awake()
        {
            Ins = this;
            input = new NewInput();
            input.EscAction += InputEsc;
        }

        public void InputEsc()
        {
            esc = !esc;
            Cursor.lockState = !esc ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = esc;
        }
    }
}