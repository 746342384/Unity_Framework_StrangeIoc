using Base.UI;
using Framework.framework.repository;
using framework.framework.ui.api;
using Module.Start;
using strange.extensions.command.impl;
using UnityEngine;

namespace Root.Commands
{
    public class PreLoadResourcesCommand : EventCommand
    {
        [Inject] public IPanelSystem PanelSystem { get; set; }
        [Inject] public IRepositoryManager RepositoryManager { get; set; }

        public override async void Execute()
        {
            Debug.Log("PreLoadResources");
            await PanelSystem.PreLoadPanel();
            await RepositoryManager.LoadRepositories();
            Dispatcher.Dispatch(StartEvent.PreLoadResourcesComplete);
        }
    }
}