﻿using Base.UI;
using framework.framework.ui.api;
using strange.extensions.command.impl;

namespace Root.Commands
{
    public class StartCommand : EventCommand
    {
        [Inject] public IPanelSystem PanelSystem { get; set; }

        public override void Execute()
        {
            PanelSystem.OpenPanel(PanelNames.StartPanel);
        }
    }
}