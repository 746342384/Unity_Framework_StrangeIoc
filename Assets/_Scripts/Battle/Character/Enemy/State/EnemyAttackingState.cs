using _Scripts.Battle.Character.Property;
using Battle.Character;
using UnityEngine;

namespace Battle.Enemy.State
{
    public class EnemyAttackingState : EnemyStateBase
    {
        private readonly int _attackIndex;
        private AttackData _attackData;

        public EnemyAttackingState(EnemyBase enemyBase, int attackIndex) : base(enemyBase)
        {
            _attackIndex = attackIndex;
        }

        public override void Enter()
        {
            _attackData = EnemyBase.CharacterData.AttackDatas[_attackIndex];
            EnemyBase.WeaponBase.ClearTargets();
            EnemyBase.Animator.CrossFadeInFixedTime(_attackData.AnimationName, 0.1f);
        }

        public override void Tick(float deltaTime)
        {
            if (!HasTarget() || EnemyBase.Target.IsDead)
            {
                EnemyBase.WeaponBase.EndAttack();
                EnemyBase.StateMachine.SwitchState(new EnemyIdleState(EnemyBase));
                return;
            }

            EnemyBase.AIMoveComponent.Move(Vector3.zero, deltaTime);
            var attackDis = EnemyBase.Attribute.GetAttributeValue(AttributeName.AttackDistance);
            if (GetDistance(EnemyBase.Target.transform.position) > attackDis)
            {
                EnemyBase.StateMachine.SwitchState(new EnemyIdleState(EnemyBase));
                return;
            }

            var normalizedTime = GetNormalizedTime(EnemyBase.Animator);
            if (normalizedTime >= _attackData.AttackStart)
            {
                EnemyBase.WeaponBase.StartAttack();
            }

            if (normalizedTime >= _attackData.AttackEnd)
            {
                EnemyBase.WeaponBase.EndAttack();
            }
        }

        public override void Exit()
        {
        }
    }
}