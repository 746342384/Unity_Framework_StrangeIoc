namespace _Scripts.Battle.Character.Skill.GreatSkill
{
    public enum GreatSkillEffectName
    {
        Play_Effect,
        Play_Audio,
        Start_Attack,
        End_Attack,
        Add_Force,
        Create_Bullet,
        Modify_Property,
        Add_Buff
    }
}