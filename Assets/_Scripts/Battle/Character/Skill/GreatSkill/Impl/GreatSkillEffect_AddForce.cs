using System;
using Battle.Character.Base;
using Framework.framework.attribute;
using UnityEngine;

namespace _Scripts.Battle.Character.Skill.GreatSkill.Impl
{
    [Act(typeof(IGreatSkillEffect), GreatSkillEffectName.Add_Force)]
    public class GreatSkillEffect_AddForce : GreatSkillEffectBase
    {
        public override void Execute(CharacterBase characterBase, object data)
        {
            if (data is not AddForceArg arg) return;
            characterBase.ReceiveForceComponent.HittedForce(arg.AddForce, arg.Duration, arg.Speed);
        }
    }

    [Serializable]
    public class AddForceArg
    {
        public Vector3 AddForce;
        public float Duration;
        public float Speed;
    }
}