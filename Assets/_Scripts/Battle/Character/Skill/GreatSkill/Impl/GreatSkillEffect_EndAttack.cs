using System;
using Battle.Character.Base;
using Framework.framework.attribute;

namespace _Scripts.Battle.Character.Skill.GreatSkill.Impl
{
    [Act(typeof(IGreatSkillEffect), GreatSkillEffectName.End_Attack)]
    public class GreatSkillEffect_EndAttack : GreatSkillEffectBase
    {
        public override void Execute(CharacterBase characterBase, object data)
        {
            if (data is not EndAttackArg arg) return;
            characterBase.WeaponBase.EndAttack();
        }
    }

    [Serializable]
    public class EndAttackArg
    {
    }
}