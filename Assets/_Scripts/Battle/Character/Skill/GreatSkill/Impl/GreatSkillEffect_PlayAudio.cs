using System;
using Battle.Character.Base;
using Framework.framework.attribute;
using Framework.framework.sound;
using UnityEngine;

namespace _Scripts.Battle.Character.Skill.GreatSkill.Impl
{
    [Act(typeof(IGreatSkillEffect), GreatSkillEffectName.Play_Audio)]
    public class GreatSkillEffect_PlayAudio : GreatSkillEffectBase
    {
        public override void Execute(CharacterBase characterBase, object data)
        {
            if (data is not PlayAudioArg arg) return;
            var soundManager = GameContext.Instance.GetService<ISoundManager>();
            soundManager.PlaySfx(arg.AudioClip);
        }
    }

    [Serializable]
    public class PlayAudioArg
    {
        public AudioClip AudioClip;
    }
}