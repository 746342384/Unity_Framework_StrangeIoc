using System;
using Battle.Character.Base;
using Framework.framework.attribute;
using UnityEngine;

namespace _Scripts.Battle.Character.Skill.GreatSkill.Impl
{
    [Act(typeof(IGreatSkillEffect), GreatSkillEffectName.Play_Effect)]
    public class GreatSkillEffect_PlayEffect : GreatSkillEffectBase
    {
        public override void Execute(CharacterBase characterBase, object data)
        {
            if (data is not PlayerEffectArg arg) return;
            var effect = UnityEngine.Object.Instantiate(arg.Effect, characterBase.transform);
            effect.transform.localPosition = arg.Offset;
            UnityEngine.Object.Destroy(effect, arg.AliveTime);
        }
    }

    [Serializable]
    public class PlayerEffectArg
    {
        public float AliveTime;
        public GameObject Effect;
        public Vector3 Offset;
    }
}