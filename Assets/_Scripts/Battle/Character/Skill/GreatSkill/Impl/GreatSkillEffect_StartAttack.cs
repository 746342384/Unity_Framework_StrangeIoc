using System;
using Battle.Character.Base;
using Framework.framework.attribute;

namespace _Scripts.Battle.Character.Skill.GreatSkill.Impl
{
    [Act(typeof(IGreatSkillEffect), GreatSkillEffectName.Start_Attack)]
    public class GreatSkillEffect_StartAttack : GreatSkillEffectBase
    {
        public override void Execute(CharacterBase characterBase, object data)
        {
            if (data is not StartAttackArg arg) return;
            characterBase.WeaponBase.StartAttack();
        }
    }

    [Serializable]
    public class StartAttackArg
    {
    }
}