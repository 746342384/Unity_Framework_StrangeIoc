using Battle.Character.Base;

namespace _Scripts.Battle.Character.Skill.GreatSkill
{
    public abstract class GreatSkillEffectBase : IGreatSkillEffect
    {
        public abstract void Execute(CharacterBase characterBase, object data);
    }
}