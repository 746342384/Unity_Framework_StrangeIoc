using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.framework.attribute;

namespace _Scripts.Battle.Character.Skill.GreatSkill
{
    public static class GreatSkillEffectFactory
    {
        private static readonly Dictionary<GreatSkillEffectName, GreatSkillEffectBase> _cache = new();

        public static GreatSkillEffectBase GetGreatSkillEffect(GreatSkillEffectName name)
        {
            if (_cache.TryGetValue(name, out var effectBase))
            {
                return effectBase;
            }

            var assembly = Assembly.GetAssembly(typeof(GreatSkillEffectMark));
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                var customAttribute = type.GetCustomAttribute<ActAttribute>();
                if (customAttribute == null) continue;

                if (customAttribute.Tag != typeof(IGreatSkillEffect) ||
                    customAttribute.Key is not GreatSkillEffectName effectName ||
                    !effectName.Equals(name)) continue;

                effectBase = Activator.CreateInstance(type) as GreatSkillEffectBase;
                _cache[name] = effectBase;
                return effectBase;
            }

            throw new Exception($"not found GreatSkillEffect name:{name.ToString()}");
        }
    }
}