using Battle.Character.Base;

namespace _Scripts.Battle.Character.Skill.GreatSkill
{
    public interface IGreatSkillEffect
    {
        void Execute(CharacterBase characterBase,object data);
    }
}