using System;
using UnityEngine;

namespace Battle.Character
{
    [Serializable]
    [CreateAssetMenu(fileName = nameof(TrailData), menuName = "Battle/TrailData", order = 1)]
    public class TrailData : ScriptableObject
    {
        public float StartWidth;
        public float EndWidth;
        public float Time;
        public Material Material;
    }
}