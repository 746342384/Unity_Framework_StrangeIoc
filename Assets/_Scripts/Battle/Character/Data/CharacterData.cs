using System.Collections.Generic;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = nameof(CharacterData), menuName = "Battle/CharacterData", order = 1)]
    public class CharacterData : ScriptableObject
    {
        public float HP;

        #region 武器

        [Header("武器配置")] public WeaponData WeaponData;

        #endregion

        [Space(20)]

        #region 攻击

        [Header("角色普通攻击")]
        public List<AttackData> AttackDatas = new();

        [Header("基础物理攻击")] public float PhysicAttack;
        [Header("寻敌范围")] public float FindPathDistance = 5f;
        [Header("攻击范围")] public float AttackDistance = 1f;

        #endregion

        [Space(20)]

        #region 受击

        [Header("受击动画特效")]
        public GameObject HittedEfx;

        [Header("受击动作动画名")] public string GetHitAnimationName = "GetHit";

        #endregion


        [Space(20)]

        #region 移动

        [Header("施加力平滑移动时间")]
        public float MoveDrag;

        [Header("旋转速度")] public float RotationDamping;
        [Header("行走动画名")] public string WalkAniName = "Walk";
        [Header("行走动画播放速度")] public float WalkAniSpeed = 1;
        [Header("角色行走速度")] public float WalkSpeed;
        [Header("行走音效")] public AudioClip WalkAudio;
        [Header("行走音效间隔时间")] public float WalkAudioInterval = 0.5f;
        [Header("行走音效大小")] public float WalkAudioVolume = 0.5f;

        [Header("跑步动画名")] public string RunAniName = "Run";
        [Header("跑步动画播放速度")] public float RunAniSpeed = 1;
        [Header("角色跑步速度")] public float RunSpeed;
        [Header("跑步音效")] public AudioClip RunAudio;
        [Header("跑步音效间隔时间")] public float RunAudioInterval = 0.5f;
        [Header("跑步音效大小")] public float RunAudioVolume = 0.5f;

        #endregion

        [Space(20)]

        #region 跳跃

        [Header("跳跃动作动画名")]
        public string JumpAnimationName = "Jump";

        [Header("跳跃动画播放速度")] public float JumpAnimationSpeed = 1;
        [Header("进入跳跃动作过渡时间")] [Range(0, 1)] public float JumpFixedTransitionDuration = 0.2f;
        [Header("跳跃结束时间")] [Range(0, 1)] public float JumpEndTime = 1f;
        [Header("跳跃施加力开始时间")] [Range(0, 1)] public float JumpAddForceStartTime;
        [Header("跳跃施加力")] public Vector3 JumpAddForce;
        [Header("跳跃施加力持续时间")] public float JumpAddForceDuration;

        #endregion

        #region 技能

        [Header("角色大招技能")] public SkillData SkillData;

        #endregion
    }
}