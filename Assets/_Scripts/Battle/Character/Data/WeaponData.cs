using Battle.Character.Weapon;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = nameof(WeaponData), menuName = "Battle/WeaponData", order = 1)]
    public class WeaponData : ScriptableObject
    {
        public AttackType AttackType;
        [Header("顿帧时间")] public float AttackStopStep;
        [Header("武器拖尾效果")] public TrailData TrailData;
        [Header("武器攻击敌人,敌人受击的力")] public Vector3 WeaponForce;
        [Header("武器攻击施加受击力持续时间")]public float WeaponForceDuration = 0.2f;
        [Header("武器攻击施加受击力速度")] public float WeaponForceSpeed;
    }
}