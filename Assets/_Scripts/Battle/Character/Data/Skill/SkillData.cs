using System.Collections.Generic;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = "SkillData", menuName = "Battle/SkillData", order = 0)]
    public class SkillData : ScriptableObject
    {
        [Header("技能动画名")] public string AnimationName;
        [Header("技能动画进入过渡时间")] public float CrossFadeInFixedTime;
        [Header("技能事件序列")] public List<GreatSkillEffectDataBase> SkillEffects = new();
    }
}