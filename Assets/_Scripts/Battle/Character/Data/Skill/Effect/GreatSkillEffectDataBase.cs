using _Scripts.Battle.Character.Skill.GreatSkill;
using UnityEngine;

namespace Battle.Character
{
    public abstract class GreatSkillEffectDataBase: ScriptableObject
    {
        public GreatSkillEffectName EffectName;
        public float StartTime;
        public abstract object GetArg();
    }
}