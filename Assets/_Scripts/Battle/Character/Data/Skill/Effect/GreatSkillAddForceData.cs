using _Scripts.Battle.Character.Skill.GreatSkill.Impl;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = "AddForce", menuName = "Battle/GreatSkillEffect/AddForce", order = 0)]
    public class GreatSkillAddForceData : GreatSkillEffectDataBase
    {
        public AddForceArg Arg;

        public override object GetArg()
        {
            return Arg;
        }
    }
}