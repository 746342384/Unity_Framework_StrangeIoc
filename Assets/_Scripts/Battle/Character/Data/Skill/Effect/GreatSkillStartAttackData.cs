using _Scripts.Battle.Character.Skill.GreatSkill.Impl;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = "StartAttack", menuName = "Battle/GreatSkillEffect/StartAttack", order = 0)]
    public class GreatSkillStartAttackData : GreatSkillEffectDataBase
    {
        public StartAttackArg Arg;

        public override object GetArg()
        {
            return Arg;
        }
    }
}