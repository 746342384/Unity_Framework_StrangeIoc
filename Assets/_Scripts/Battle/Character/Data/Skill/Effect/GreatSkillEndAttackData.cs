using _Scripts.Battle.Character.Skill.GreatSkill.Impl;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = "EndAttack", menuName = "Battle/GreatSkillEffect/EndAttack", order = 0)]
    public class GreatSkillEndAttackData : GreatSkillEffectDataBase
    {
        public EndAttackArg Arg;
        public override object GetArg()
        {
            return Arg;
        }
    }
}