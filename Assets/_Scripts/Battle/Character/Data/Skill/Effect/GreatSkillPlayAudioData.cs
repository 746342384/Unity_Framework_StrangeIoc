using _Scripts.Battle.Character.Skill.GreatSkill.Impl;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = "PlayAudio", menuName = "Battle/GreatSkillEffect/PlayAudio", order = 0)]
    public class GreatSkillPlayAudioData : GreatSkillEffectDataBase
    {
        public PlayAudioArg Arg;

        public override object GetArg()
        {
            return Arg;
        }
    }
}