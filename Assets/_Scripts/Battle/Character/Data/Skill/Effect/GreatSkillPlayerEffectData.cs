using _Scripts.Battle.Character.Skill.GreatSkill.Impl;
using UnityEngine;

namespace Battle.Character
{
    [CreateAssetMenu(fileName = "PlayerEffect", menuName = "Battle/GreatSkillEffect/PlayerEffect", order = 0)]
    public class GreatSkillPlayerEffectData : GreatSkillEffectDataBase
    {
        public PlayerEffectArg Arg;
        public override object GetArg()
        {
            return Arg;
        }
    }
}