using _Scripts.Battle.Character.Property;
using UnityEngine;

namespace Battle.Character.Player.State
{
    public class PlayerWalkState : PlayerStateBase
    {
        private const float FixedTransitionDuration = 0.2f;
        private static readonly int MoveSpeed = Animator.StringToHash("MoveSpeed");
        private bool isJump;

        public PlayerWalkState(PlayerBase character) : base(character)
        {
        }

        public override void Enter()
        {
            base.Enter();
            SetAnimationSpeed(Character.CharacterData.WalkAniSpeed);
            Character.Animator.CrossFadeInFixedTime(Character.CharacterData.WalkAniName, FixedTransitionDuration);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
            if (Character.InputComponent.IsLookLock)
            {
                FaceToTarget();
            }

            if (Character.InputComponent.IsJumping)
            {
                Character.StateMachine.SwitchState(new PlayerJumpState(Character));
                return;
            }

            if (Character.InputComponent.IsRunning)
            {
                Character.StateMachine.SwitchState(new PlayerRunState(Character));
                return;
            }

            if (Character.InputComponent.IsAttacking)
            {
                Character.StateMachine.SwitchState(new PlayerAttackingState(Character, 0));
                return;
            }

            var vector2 = Character.MoveComponent.GetMovement();
            var movement = Character.MoveComponent.CalculateMovement(vector2);
            var walkSpeed = Character.Attribute.GetAttributeValue(AttributeName.WalkSpeed);
            Character.MoveComponent.Move(movement * walkSpeed, deltaTime);
            if (vector2 == Vector2.zero)
            {
                Character.AudioComponent.StopWalkSfx(Character.CharacterData.WalkAudio);
                Character.Animator.SetFloat(MoveSpeed, 0, 0.1f, deltaTime);
                return;
            }

            Character.Animator.SetFloat(MoveSpeed, 1, 0.1f, deltaTime);
            Character.MoveComponent.FaceMovementDirection(movement, deltaTime);
            Character.AudioComponent.PlayWalkSfx(
                Character.CharacterData.WalkAudio,
                Character.CharacterData.WalkAudioInterval,
                Character.CharacterData.WalkAudioVolume);
        }

        public override void Exit()
        {
            base.Exit();
            Character.AudioComponent.StopWalkSfx(Character.CharacterData.WalkAudio);
            ResetAnimatorSpeed();
        }
    }
}