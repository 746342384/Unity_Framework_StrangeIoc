using UnityEngine;

namespace Battle.Character.Player.State
{
    public class PlayerAttackingState : PlayerStateBase
    {
        private int _index;
        private AttackData _attackData;

        public PlayerAttackingState(PlayerBase character, int index) : base(character)
        {
            _index = index;
            character.WeaponBase.AttackDataIndex = index;
        }

        public override void Enter()
        {
            base.Enter();
            _attackData = Character.CharacterData.AttackDatas[_index];
            Character.WeaponBase.ClearTargets();
            SetAnimationSpeed(_attackData.AnimationSpeed);
            Character.Animator.CrossFadeInFixedTime(_attackData.AnimationName, 0.1f);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
            Character.MoveComponent.Move(Vector3.zero, deltaTime);

            if (Character.InputComponent.IsLookLock)
            {
                FaceToTarget();
            }

            ApplyForce(_attackData.AddForce, _attackData.AddForceDuration);

            var normalizedTime = GetNormalizedTime(Character.Animator);

            PlayAttackEfx(normalizedTime, _attackData);
            PlayAttackSfx(normalizedTime, _attackData);
            ExecuteAttact(normalizedTime, _attackData);

            if (normalizedTime > 0.5f && Character.InputComponent.CancelAttacking)
            {
                Character.StateMachine.SwitchState(new PlayerJumpState(Character));
                return;
            }

            if (normalizedTime >= 1f)
            {
                Character.StateMachine.SwitchState(new PlayerWalkState(Character));
                return;
            }

            if (normalizedTime >= _attackData.Time && Character.InputComponent.IsAttacking)
            {
                _index += 1;
                if (_index >= Character.CharacterData.AttackDatas.Count)
                {
                    return;
                }

                Character.StateMachine.SwitchState(new PlayerAttackingState(Character, _index));
            }
        }

        public override void Exit()
        {
            base.Exit();
            ResetAnimatorSpeed();
        }
    }
}