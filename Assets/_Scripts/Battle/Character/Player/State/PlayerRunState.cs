using _Scripts.Battle.Character.Property;
using UnityEngine;

namespace Battle.Character.Player.State
{
    public class PlayerRunState : PlayerStateBase
    {
        private const float FixedTransitionDuration = 0.2f;
        private static readonly int MoveSpeed = Animator.StringToHash("MoveSpeed");

        public PlayerRunState(PlayerBase character) : base(character)
        {
        }

        public override void Enter()
        {
            base.Enter();
            SetAnimationSpeed(Character.CharacterData.RunAniSpeed);
            Character.Animator.CrossFadeInFixedTime(Character.CharacterData.RunAniName, FixedTransitionDuration);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
            if (Character.InputComponent.IsLookLock)
            {
                FaceToTarget();
            }

            if (Character.InputComponent.IsJumping)
            {
                Character.StateMachine.SwitchState(new PlayerJumpState(Character));
                return;
            }

            if (Character.InputComponent.IsAttacking)
            {
                Character.StateMachine.SwitchState(new PlayerAttackingState(Character, 0));
                return;
            }

            if (!Character.InputComponent.IsRunning)
            {
                Character.StateMachine.SwitchState(new PlayerWalkState(Character));
                return;
            }

            var vector2 = Character.MoveComponent.GetMovement();
            var movement = Character.MoveComponent.CalculateMovement(vector2);
            Character.MoveComponent.Move(movement * Character.Attribute.GetAttributeValue(AttributeName.RunSpeed), deltaTime);

            if (Character.InputComponent.MoveValue == Vector2.zero)
            {
                Character.AudioComponent.StopRunSfx(Character.CharacterData.RunAudio);
                Character.Animator.SetFloat(MoveSpeed, 0, 0.1f, deltaTime);
                return;
            }

            Character.Animator.SetFloat(MoveSpeed, 1, 0.1f, deltaTime);
            Character.MoveComponent.FaceMovementDirection(movement, deltaTime);
            Character.AudioComponent.PlayRunSfx(
                Character.CharacterData.RunAudio,
                Character.CharacterData.RunAudioInterval,
                Character.CharacterData.RunAudioVolume);
        }

        public override void Exit()
        {
            base.Exit();
            Character.AudioComponent.StopRunSfx(Character.CharacterData.RunAudio);
            ResetAnimatorSpeed();
        }
    }
}