using UnityEngine;

namespace Battle.Character.Player.State
{
    public class PlayerJumpState : PlayerStateBase
    {
        private float _previousFrameTime;

        public PlayerJumpState(PlayerBase character) : base(character)
        {
        }

        public override void Enter()
        {
            base.Enter();
            if (Character.InputComponent.IsMoveForward || Character.InputComponent.IsMoveBaclward)
            {
                AddForce = Character.CharacterData.JumpAddForce;
            }
            else
            {
                AddForce = Vector3.zero;
            }

            var jumpAnimationName = Character.CharacterData.JumpAnimationName;
            var transitionDuration = Character.CharacterData.JumpFixedTransitionDuration;
            SetAnimationSpeed(Character.CharacterData.JumpAnimationSpeed);
            Character.Animator.CrossFadeInFixedTime(jumpAnimationName, transitionDuration);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
            Character.MoveComponent.Move(Vector3.zero, deltaTime);
            var normalizedTime = GetNormalizedTime(Character.Animator);

            if (normalizedTime >= Character.CharacterData.JumpAddForceStartTime && !IsApplyForce)
            {
                Character.ReceiveForceComponent.AddForce(AddForce, Character.CharacterData.JumpAddForceDuration);
                IsApplyForce = true;
            }

            if (normalizedTime > _previousFrameTime && normalizedTime >= Character.CharacterData.JumpEndTime)
            {
                Character.StateMachine.SwitchState(new PlayerWalkState(Character));
                return;
            }

            _previousFrameTime = normalizedTime;
        }

        public override void Exit()
        {
            base.Exit();
            _previousFrameTime = 0;
            ResetAnimatorSpeed();
        }
    }
}