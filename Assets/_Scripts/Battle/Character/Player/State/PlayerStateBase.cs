using System;
using Battle.Character.Base.Component;
using Battle.Character.Controller;
using Framework.framework.sound;
using UnityEngine;

namespace Battle.Character.Player.State
{
    public abstract class PlayerStateBase : StateBase
    {
        private readonly ISoundManager _soundManager;
        protected readonly PlayerBase Character;
        protected bool IsApplyForce;
        protected Vector3 AddForce;

        protected PlayerStateBase(PlayerBase character)
        {
            Character = character;
            _soundManager = GameContext.Instance.GetComponent<ISoundManager>() as ISoundManager;
        }

        public override void Enter()
        {
            Character.InputComponent.SkillAction += OnSkill;
        }

        public override void Tick(float deltaTime)
        {
        }

        public override void Exit()
        {
            Character.InputComponent.SkillAction -= OnSkill;
        }

        private void OnSkill()
        {
            if (Character.IsDead)
            {
                return;
            }

            var animatorStateInfo = Character.Animator.GetCurrentAnimatorStateInfo(0);
            var nextAnimatorStateInfo = Character.Animator.GetNextAnimatorStateInfo(0);
            if (animatorStateInfo.IsName(Character.CharacterData.JumpAnimationName) ||
                animatorStateInfo.IsName(Character.CharacterData.SkillData.AnimationName) ||
                nextAnimatorStateInfo.IsName(Character.CharacterData.SkillData.AnimationName))
            {
                return;
            }

            Character.StateMachine.SwitchState(new PlayerSkillState(Character));
        }

        protected void ApplyForce(Vector3 attackDataAddForce, float duration)
        {
            if (IsApplyForce) return;
            Character.ReceiveForceComponent.AddForce(attackDataAddForce, duration);
            IsApplyForce = true;
        }

        protected void PlayAttackSfx(float normalizedTime, AttackData attackData)
        {
            if (Math.Abs(normalizedTime - attackData.AttackSfxTime) < 0.01f)
            {
                _soundManager?.PlaySfx(attackData.AttackSfx);
            }
        }

        protected void PlayAttackEfx(float normalizedTime, AttackData attackData)
        {
            if (Math.Abs(normalizedTime - attackData.AttackEfxTime) < 0.01f)
            {
                Character.EffectComponent.PlayerAttackEfx(attackData.AttackEfx, attackData.AttackEfxDuration,
                    attackData.EffectParent);
            }
        }

        protected void ExecuteAttact(float normalizedTime, AttackData attackData)
        {
            if (normalizedTime > attackData.AttackStart)
            {
                Character.WeaponBase.StartAttack();
            }

            if (normalizedTime >= attackData.AttackEnd)
            {
                Character.WeaponBase.EndAttack();
            }
        }

        protected void SetAnimationSpeed(float speed)
        {
            Character.Animator.speed = speed;
        }

        protected void ResetAnimatorSpeed()
        {
            Character.Animator.speed = 1;
        }

        protected void FaceToTarget()
        {
            var nearEnemy = BattleController.Ins.GetNearEnemy(Character);
            if (nearEnemy == null)
            {
                return;
            }

            var lookPos = nearEnemy.transform.position - Character.transform.position;
            lookPos.y = 0;

            Character.transform.rotation = Quaternion.LookRotation(lookPos);
        }
    }
}