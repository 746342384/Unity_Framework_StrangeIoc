using System.Linq;
using _Scripts.Battle.Action;
using _Scripts.Battle.Character.Skill.GreatSkill;

namespace Battle.Character.Player.State
{
    public class PlayerSkillState : PlayerStateBase
    {
        private SkillData _skillData;
        private ActionBasic _actions;

        public PlayerSkillState(PlayerBase character) : base(character)
        {
        }

        public override void Enter()
        {
            base.Enter();
            _skillData = Character.CharacterData.SkillData;
            Character.Animator.CrossFadeInFixedTime(_skillData.AnimationName, _skillData.CrossFadeInFixedTime);
            var animatorStateInfo = Character.Animator.GetCurrentAnimatorStateInfo(0);
            if (animatorStateInfo.IsName(_skillData.AnimationName)) return;

            _actions = new ActionBasic();
            var skillEffectDatas = _skillData.SkillEffects
                .OrderBy(s => s.StartTime)
                .ToList();

            var previourTime = 0f;
            
            foreach (var skillEffectData in skillEffectDatas)
            {
                _actions.AddActionWait(skillEffectData.StartTime - previourTime);
                _actions.AddActionDelegate(() => { OnEffect(skillEffectData); });
                previourTime = skillEffectData.StartTime;
            }

            _actions.Init();
        }

        private void OnEffect(GreatSkillEffectDataBase skillEffectData)
        {
            var skillEffect = GreatSkillEffectFactory.GetGreatSkillEffect(skillEffectData.EffectName);
            skillEffect.Execute(Character, skillEffectData.GetArg());
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
            var normalizedTime = GetNormalizedTime(Character.Animator);
            if (normalizedTime >= 1)
            {
                Character.StateMachine.SwitchState(new PlayerWalkState(Character));
                return;
            }
        }
    }
}