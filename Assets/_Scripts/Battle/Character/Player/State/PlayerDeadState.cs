namespace Battle.Character.Player.State
{
    public class PlayerDeadState : PlayerStateBase
    {
        public PlayerDeadState(PlayerBase character) : base(character)
        {
        }

        public override void Enter()
        {
            base.Enter();
            Character.Animator.CrossFadeInFixedTime("Dead", 0.1f);
        }
    }
}