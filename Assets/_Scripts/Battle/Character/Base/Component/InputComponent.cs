using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Battle.Character.Base.Component
{
    public class InputComponent : MonoBehaviour, PlayerInput.IPlayerActions
    {
        private PlayerInput PlayerInput;
        public Vector2 MoveValue { get; private set; }
        public bool IsAttacking { get; private set; }
        public bool IsTwoHandAttacking { get; private set; }
        public bool CancelAttacking { get; private set; }
        public bool IsMoveForward { get; private set; }
        public bool IsMoveBaclward { get; private set; }
        public bool IsRunning { get; private set; }
        public bool IsJumping { get; private set; }
        public bool IsLookLock { get; private set; }
        public event Action SkillAction;

        private void Start()
        {
            PlayerInput = new PlayerInput();
            PlayerInput.Player.SetCallbacks(this);
            PlayerInput.Enable();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            MoveValue = context.ReadValue<Vector2>();
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                CancelAttacking = true;
                IsJumping = true;
            }

            if (context.canceled)
            {
                IsJumping = false;
            }
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                CancelAttacking = false;
                IsAttacking = true;
            }

            if (context.canceled)
            {
                IsAttacking = false;
            }
        }

        public void OnTwoHandAttack(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                CancelAttacking = false;
                IsTwoHandAttacking = true;
            }

            if (context.canceled)
            {
                IsTwoHandAttacking = false;
            }
        }

        public void OnIsMoveForward(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                IsMoveForward = true;
            }

            if (context.canceled)
            {
                IsMoveForward = false;
            }
        }

        public void OnIsMoveBackward(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                IsMoveBaclward = true;
            }

            if (context.canceled)
            {
                IsMoveBaclward = false;
            }
        }

        public void OnFreeLook(InputAction.CallbackContext context)
        {
        }

        public void OnRun(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                IsRunning = true;
            }

            if (context.canceled)
            {
                IsRunning = false;
            }
        }

        public void OnLookLock(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                IsLookLock = !IsLookLock;
            }
        }

        public void OnSkill(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                SkillAction?.Invoke();
            }
        }

        private void OnDisable()
        {
            PlayerInput.Disable();
        }
    }
}