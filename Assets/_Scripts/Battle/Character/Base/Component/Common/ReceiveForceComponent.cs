using UnityEngine;
using UnityEngine.AI;

namespace Battle.Character.Base.Component
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class ReceiveForceComponent : MonoBehaviour
    {
        private CharacterBase _character;
        private NavMeshAgent Agent;
        private float _verticalVelocity;
        private Vector3 _dampingVelocity;
        private Vector3 _impact;
        private bool _isHitted;
        private float _addForceDuration;
        private float _hittedForceDuration;
        private float _hittedForceSpeed;
        private Vector3 _hittedTargetPos;
        public Vector3 MoveMent => _impact + Vector3.up * _verticalVelocity;

        public void Init(CharacterBase characterBase)
        {
            _character = characterBase;
        }

        private void Awake()
        {
            Agent = GetComponent<NavMeshAgent>();
        }

        private void FixedUpdate()
        {
            if (_character == null) return;
            if (_verticalVelocity < 0 && _character.CharacterController.isGrounded)
            {
                _verticalVelocity = Physics.gravity.y * Time.deltaTime;
            }
            else
            {
                _verticalVelocity += Physics.gravity.y * Time.deltaTime;
            }


            if (_isHitted)
            {
                _impact = Vector3.SmoothDamp(_impact, Vector3.zero, ref _dampingVelocity, _hittedForceDuration);

                if (_impact.sqrMagnitude < 0.2f * 0.2f && Agent)
                {
                    _isHitted = false;
                    _impact = Vector3.zero;
                    Agent.enabled = true;
                }

                _character.CharacterController.Move(_impact * Time.fixedDeltaTime * _hittedForceSpeed);
            }
            else
            {
                _impact = Vector3.SmoothDamp(_impact, Vector3.zero, ref _dampingVelocity, _addForceDuration);

                if (_impact.sqrMagnitude < 0.2f * 0.2f && Agent)
                {
                    _impact = Vector3.zero;
                    Agent.enabled = true;
                }
            }
        }

        public void AddForce(Vector3 force, float duration)
        {
            _addForceDuration = duration;
            var trans = _character.transform;
            var xVec = trans.right.normalized * force.x;
            var yVec = trans.up.normalized * force.y;
            var zVec = trans.forward.normalized * force.z;
            _impact += xVec + yVec + zVec;
            if (Agent) Agent.enabled = false;
        }


        public void HittedForce(Vector3 hitForce, float duration, float speed)
        {
            _isHitted = true;
            if (Agent) Agent.enabled = false;
            _hittedForceDuration = duration;
            _hittedForceSpeed = speed;
            var trans = _character.transform;
            var xVec = trans.right.normalized * hitForce.x;
            var yVec = trans.up.normalized * hitForce.y;
            var zVec = trans.forward.normalized * hitForce.z;
            _impact += xVec + yVec + zVec;
        }
    }
}