using UnityEngine;

namespace Battle.Character.Base.Component
{
    [RequireComponent(typeof(TrailRenderer))]
    public class TrailComponent : MonoBehaviour
    {
        private TrailRenderer _trailRenderer;
        private TrailData _data;

        private void Awake()
        {
            _trailRenderer = GetComponent<TrailRenderer>();
            DisableTrail();
        }

        public void Init(TrailData data)
        {
            _data = data;
            if (data == null) return;
            _trailRenderer.startWidth = data.StartWidth;
            _trailRenderer.endWidth = data.EndWidth;
            _trailRenderer.material = data.Material;
            _trailRenderer.time = data.Time;
        }

        public void EnableTrail()
        {
            if (_data == null) return;
            _trailRenderer.enabled = true;
        }

        public void DisableTrail()
        {
            _trailRenderer.enabled = false;
        }
    }
}