using Framework.framework.sound;
using UnityEngine;

namespace Battle.Character.Base.Component
{
    public class AudioComponent : MonoBehaviour
    {
        private ISoundManager _soundManager;

        private AudioClip _walkClip;
        private bool _isPlayingWalk;
        private float _walkTime;
        private float _walkInterval;
        private float _walkVolume;

        private AudioClip _runClip;
        private bool _isPlayingRun;
        private float _runTime;
        private float _runInterval;
        private float _runVolume;

        public void Init()
        {
            _soundManager = GameContext.Instance.GetService<ISoundManager>();
        }

        public void PlayWalkSfx(AudioClip clip, float walkInterval, float walkVolume)
        {
            _walkClip = clip;
            _walkVolume = walkVolume;
            _walkInterval = walkInterval;
            if (_isPlayingWalk) return;
            _isPlayingWalk = true;
            _walkTime = Time.time;
        }

        public void PlayRunSfx(AudioClip runClip, float runAudioInterval, float runAudioVolume)
        {
            _runClip = runClip;
            _runVolume = runAudioVolume;
            _runInterval = runAudioInterval;
            if (_isPlayingRun) return;
            _isPlayingRun = true;
            _runTime = Time.time;
        }

        public void StopWalkSfx(AudioClip clip)
        {
            if (!_isPlayingWalk) return;
            _isPlayingWalk = false;
            _soundManager.StopSfx(clip);
        }

        public void StopRunSfx(AudioClip runClip)
        {
            if (!_isPlayingRun) return;
            _isPlayingRun = false;
            _soundManager.StopSfx(runClip);
        }

        private void Update()
        {
            UpdateWalk();
            UpdateRun();
        }

        private void UpdateRun()
        {
            if (!_isPlayingRun || Time.time - _runTime <= _runInterval) return;
            _soundManager.PlaySfx(_runClip, false, _runVolume);
            _runTime = Time.time;
        }

        private void UpdateWalk()
        {
            if (!_isPlayingWalk || Time.time - _walkTime <= _walkInterval) return;
            _soundManager.PlaySfx(_walkClip, false, _walkVolume);
            _walkTime = Time.time;
        }
    }
}