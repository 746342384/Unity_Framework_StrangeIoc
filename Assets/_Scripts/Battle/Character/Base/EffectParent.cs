namespace Battle.Character.Base
{
    public enum EffectParent
    {
        Top,
        Middle,
        Bottom,
        Head,
        LeftHand,
        RightHand,
        LeftFoot,
        RightFoot
    }
}