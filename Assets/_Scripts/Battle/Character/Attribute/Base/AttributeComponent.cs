using System.Collections.Generic;
using Battle.Character;

namespace _Scripts.Battle.Character.Property
{
    public class AttributeComponent
    {
        private readonly Dictionary<string, Attribute> attributes;

        public AttributeComponent()
        {
            attributes = new Dictionary<string, Attribute>();
        }

        public void Init(CharacterData characterData)
        {
            attributes.Clear();
            AddAttribute("MaxHp", characterData.HP);
            AddAttribute("RunSpeed", characterData.RunSpeed);
            AddAttribute("WalkSpeed", characterData.WalkSpeed);
            AddAttribute("AttackDistance", characterData.AttackDistance);
            AddAttribute("PhysicAttack", characterData.PhysicAttack);
            AddAttribute("Hp", characterData.HP);
        }

        public void AddAttribute(string attributeName, float baseValue)
        {
            attributes[attributeName] = new Attribute(baseValue);
        }

        public void RemoveAttribute(string attributeName)
        {
            attributes.Remove(attributeName);
        }

        public float GetAttributeValue(string attributeName)
        {
            return attributes.TryGetValue(attributeName, out var attribute) ? attribute.GetValue() : 0f;
        }

        public float GetAttributeBaseValue(string attributeName)
        {
            return attributes.TryGetValue(attributeName, out var attribute) ? attribute.GetBaseValue() : 0f;
        }

        public float GetAttributeBaseValue(AttributeName attributeName)
        {
            return attributes.TryGetValue(attributeName.ToString(), out var attribute) ? attribute.GetBaseValue() : 0f;
        }


        public float GetAttributeValue(AttributeName attributeName)
        {
            return GetAttributeValue(attributeName.ToString());
        }

        public void ApplyBonus(AttributeName attributeName, float bonus)
        {
            if (attributes.TryGetValue(attributeName.ToString(), out var attribute))
            {
                attribute.AddBonus(bonus);
            }
        }

        public void ResetAttribute(AttributeName attributeName)
        {
            if (attributes.TryGetValue(attributeName.ToString(), out var attribute))
            {
                attribute.Reset();
            }
        }

        public void ApplyBonus(string attributeName, float bonus)
        {
            if (attributes.TryGetValue(attributeName, out var attribute))
            {
                attribute.AddBonus(bonus);
            }
        }

        public void RemoveBonus(string attributeName, float bonus)
        {
            if (attributes.TryGetValue(attributeName, out var attribute))
            {
                attribute.RemoveBonus(bonus);
            }
        }

        public void ApplyPercentBonus(string attributeName, float percent)
        {
            if (attributes.TryGetValue(attributeName, out var attribute))
            {
                attribute.AddPercentBonus(percent);
            }
        }

        public void RemovePercentBonus(string attributeName, float percent)
        {
            if (attributes.TryGetValue(attributeName, out var attribute))
            {
                attribute.RemovePercentBonus(percent);
            }
        }
        
        public void ApplyModifier(string attributeName, AttributeModifier modifier)
        {
            if (attributes.TryGetValue(attributeName, out var attribute))
            {
                attribute.ApplyModifier(modifier);
            }
        }

        public void RemoveModifier(string attributeName, AttributeModifier modifier)
        {
            if (attributes.TryGetValue(attributeName, out var attribute))
            {
                attribute.RemoveModifier(modifier);
            }
        }
    }
}