namespace _Scripts.Battle.Character.Property
{
    public abstract class AttributeModifier
    {
        public abstract float ApplyModifier(float value);
    }
}