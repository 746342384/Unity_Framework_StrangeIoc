namespace _Scripts.Battle.Character.Property
{
    public enum AttributeName
    {
        Hp,
        MaxHp,
        RunSpeed,
        WalkSpeed,
        AttackDistance,
        PhysicAttack,
    }
}