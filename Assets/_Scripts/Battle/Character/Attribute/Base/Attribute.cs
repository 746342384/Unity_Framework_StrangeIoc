using System.Collections.Generic;

namespace _Scripts.Battle.Character.Property
{
    public class Attribute
    {
        private readonly float baseValue;
        private float bonusValue;
        private float percentBonus;
        private readonly List<AttributeModifier> modifiers;

        public Attribute(float baseValue)
        {
            this.baseValue = baseValue;
            bonusValue = 0f;
            percentBonus = 0f;
            modifiers = new List<AttributeModifier>();
        }

        public float GetValue()
        {
            var value = baseValue + bonusValue;
            value *= 1f + percentBonus;

            foreach (var modifier in modifiers)
            {
                value = modifier.ApplyModifier(value);
            }
            
            return value;
        }

        public float GetBaseValue()
        {
            return baseValue;
        }

        public void AddBonus(float bonus)
        {
            bonusValue += bonus;
        }

        public void RemoveBonus(float bonus)
        {
            bonusValue -= bonus;
        }

        public void AddPercentBonus(float percent)
        {
            percentBonus += percent;
        }

        public void RemovePercentBonus(float percent)
        {
            percentBonus -= percent;
        }
        
        public void ApplyModifier(AttributeModifier modifier)
        {
            modifiers.Add(modifier);
        }

        public void RemoveModifier(AttributeModifier modifier)
        {
            modifiers.Remove(modifier);
        }

        public void Reset()
        {
            bonusValue = 0f;
            percentBonus = 0f;
            modifiers.Clear();
        }
    }
}