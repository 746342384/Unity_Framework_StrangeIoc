namespace _Scripts.Battle.Character.Property
{
    public class PropertyBase
    {
        public PropertyBase(float baseValue, float percent = 1)
        {
            _baseValue = baseValue;
            _percent = percent;
        }

        public float Value => (_baseValue + _extraValue) * _percent;
        private float _baseValue;
        private float _extraValue;
        private float _percent;

        public float AddBaseValue(float addValue)
        {
            _baseValue += addValue;
            return _baseValue;
        }

        public float CutBaseValue(float cutValue)
        {
            _baseValue -= cutValue;
            return _baseValue < 0 ? 0 : _baseValue;
        }

        public float AddExtraValue(float addValue)
        {
            _extraValue += addValue;
            return _extraValue;
        }

        public float CutExtraValue(float cutValue)
        {
            _extraValue -= cutValue;
            return _extraValue < 0 ? 0 : _extraValue;
        }

        public float AddPercent(float addValue)
        {
            _percent += addValue;
            return _percent;
        }

        public float CutPercent(float cutValue)
        {
            _percent -= cutValue;
            return _percent < 0 ? 0 : _percent;
        }
    }
}