using System.Collections.Generic;
using Battle.Character.Base;
using Battle.Character.Base.Component;
using UnityEngine;

namespace Battle.Character.Weapon
{
    [RequireComponent(typeof(BezierSpline))]
    public class WeaponBase : MonoBehaviour
    {
        private bool _isAttacking;
        private CharacterBase CharacterBase;
        private TrailComponent trailComponent;

        public int AttackDataIndex;
        public Collider Collider;
        public Transform TrailPoint;
        public BezierSpline Spline;
        public List<CharacterBase> TarGets = new();

        private void Awake()
        {
            Spline = GetComponent<BezierSpline>();
            Collider = GetComponent<Collider>();
        }

        public void Init(CharacterBase characterBase)
        {
            CharacterBase = characterBase;
            InitTrail();
        }

        private void InitTrail()
        {
            if (TrailPoint == null) return;
            trailComponent = TrailPoint.gameObject.AddComponent<TrailComponent>();
            trailComponent.Init(CharacterBase.CharacterData.WeaponData.TrailData);
        }

        public void StartAttack()
        {
            Collider.enabled = true;
            if (trailComponent != null) trailComponent.EnableTrail();
        }

        public void EndAttack()
        {
            Collider.enabled = false;
            if (trailComponent != null) trailComponent.DisableTrail();
            ClearTargets();
        }

        public void ClearTargets()
        {
            TarGets.Clear();
        }

        private void OnCollisionEnter(Collision collision)
        {
            var characterBase = collision.collider.gameObject.GetComponent<CharacterBase>();
            if (characterBase != null && characterBase != CharacterBase)
            {
                AddTarget(characterBase, collision.contacts[0].point);
            }
        }

        private void PerformAttack()
        {
            var mask = LayerMask.GetMask(CharacterBase.CharacterType == CharacterType.Enemy ? "Player" : "Enemy");
            var numOfRays = Spline.points.Count - 1;
            for (var i = 0; i < numOfRays; i++)
            {
                var rayOrigin = Spline.points[i].position;
                var rayNext = Spline.points[i + 1].position;
                var bladeDirection = (rayNext - rayOrigin).normalized;
                var dir = Vector3.Distance(rayOrigin, rayNext);
                if (Physics.Raycast(rayOrigin, bladeDirection, out var hit, dir, mask))
                {
                    var characterBase = hit.collider.gameObject.GetComponent<CharacterBase>();
                    if (characterBase != null)
                    {
                        Time.timeScale = CharacterBase.CharacterData.WeaponData.AttackStopStep;
                        AddTarget(characterBase, hit);
                        Debug.DrawRay(rayOrigin, bladeDirection * dir, Color.blue, 1.0f);
                        break;
                    }
                }

                Debug.DrawRay(rayOrigin, bladeDirection * dir, Color.blue, 1.0f);
            }
        }

        private void AddTarget(CharacterBase characterBase, RaycastHit raycastHit)
        {
            AddTarget(characterBase, raycastHit.point);
        }

        private void AddTarget(CharacterBase characterBase, Vector3 point)
        {
            if (TarGets.Contains(characterBase)) return;
            TarGets.Add(characterBase);
            TakeDamage(characterBase, point);
            PlayAttackEfx(point);
        }

        private void PlayAttackEfx(Vector3 raycastHitPoint)
        {
            // const string path = "Assets/ResPackage/Common/Prefab/Effect/Sphere.prefab";
            // _characterBase.EffectComponent.PlayerAttackEfxAsync(path, null,
            //     raycastHitPoint);
        }

        private void TakeDamage(CharacterBase enemyBase, Vector3 raycastHitPoint)
        {
            switch (CharacterBase.CharacterData.WeaponData.AttackType)
            {
                case AttackType.Single:
                    SingleTakeDamage(enemyBase, raycastHitPoint);
                    break;
            }
        }

        private void SingleTakeDamage(CharacterBase enemyBase, Vector3 raycastHitPoint)
        {
            enemyBase.SingleTakeDamage(CharacterBase, AttackDataIndex, raycastHitPoint);
        }
    }
}