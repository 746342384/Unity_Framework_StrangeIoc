namespace Battle.AI.Const
{
    public enum AIType
    {
        Melee,
        Ranged,
        Magic
    }
}