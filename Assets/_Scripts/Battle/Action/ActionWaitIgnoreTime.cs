using Dxx.Util;

namespace _Scripts.Battle.Action
{
    public class ActionWaitIgnoreTime : ActionBase
    {
        protected override void OnInit()
        {
            startTime = Updater.unscaleAliveTime;
        }

        protected override void OnUpdate()
        {
            if (Updater.unscaleAliveTime - startTime >= waitTime)
            {
                End();
            }
        }

        protected override void OnForceEnd()
        {
        }

        private float startTime;

        public float waitTime;
    }
}
