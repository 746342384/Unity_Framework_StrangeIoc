using Dxx.Util;

namespace _Scripts.Battle.Action
{
    public class ActionWait : ActionBase
    {
        protected override void OnInit()
        {
            startTime = Updater.AliveTime;
        }

        protected override void OnUpdate()
        {
            if (Updater.AliveTime - startTime >= waitTime)
            {
                End();
            }
        }

        protected override void OnForceEnd()
        {
        }

        private float startTime;

        public float waitTime;

        public bool ignoreTime;
    }
}