using System.Collections.Generic;
using Dxx.Util;

namespace _Scripts.Battle.Action
{
    public class ActionBasic
    {
        public void Init(bool IgnoreTimeScale = false)
        {
            _ignoreTimeScale = IgnoreTimeScale;
            Updater.AddUpdate("ActionBasic", OnUpdate, IgnoreTimeScale);
            OnInit();
        }

        protected virtual void OnInit()
        {
        }

        public void DeInit()
        {
            OnDeInit();
            ActionClear();
            Updater.RemoveUpdate("ActionBasic", OnUpdate);
        }

        protected virtual void OnDeInit()
        {
        }

        protected virtual void OnUpdate(float delta)
        {
            if (_actionList.Count <= 0)
                return;

            _updateAction = _actionList[0];
            _updateAction.Init();
            _updateAction.Update();
            if (_updateAction.IsEnd)
            {
                if (_actionList.Count > 0)
                {
                    _actionList.RemoveAt(0);
                }

                _actionCount = _actionList.Count;
            }
        }

        public void AddAction(ActionBase action)
        {
            _actionList.Add(action);
            _actionCount = _actionList.Count;
        }

        public void AddActionWait(float waitTime)
        {
            ActionWait action = new ActionWait
            {
                waitTime = waitTime
            };
            AddAction(action);
        }

        public void AddActionIgnoreWait(float waitTime)
        {
            ActionWaitIgnoreTime action = new ActionWaitIgnoreTime
            {
                waitTime = waitTime
            };
            AddAction(action);
        }

        public void AddActionDelegate(System.Action a)
        {
            ActionDelegate action = new ActionDelegate
            {
                action = a
            };
            AddAction(action);
        }

        public void AddActionWaitDelegate(float waitTime, System.Action a)
        {
            AddActionWait(waitTime);
            AddActionDelegate(a);
        }

        public void AddActionIgnoreWaitDelegate(float waitTime, System.Action a)
        {
            AddActionIgnoreWait(waitTime);
            AddActionDelegate(a);
        }

        public void ActionClear()
        {
            _actionList.Clear();
            _actionCount = 0;
            _actionIndex = 0;
            OnActionClear();
        }

        protected virtual void OnActionClear()
        {
        }

        public int GetActionCount()
        {
            return _actionCount;
        }

        protected List<ActionBase> _actionList = new();

        protected int _actionCount;

        protected int _actionIndex;

        private bool _ignoreTimeScale;

        private ActionBase _updateAction;
    }
}