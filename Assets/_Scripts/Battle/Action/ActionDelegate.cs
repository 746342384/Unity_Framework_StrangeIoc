using System;

namespace _Scripts.Battle.Action
{
    public class ActionDelegate : ActionBase
    {
        protected override void OnInit()
        {
            if (action != null)
            {
                action();
            }

            if (actionbool != null)
            {
                actionbool(resultbool);
            }

            if (actionint != null)
            {
                actionint(resultint);
            }

            if (actionstring != null)
            {
                actionstring(resultstring);
            }

            End();
        }

        protected override void OnForceEnd()
        {
        }

        public System.Action action;

        public Action<bool> actionbool;

        public bool resultbool;

        public Action<int> actionint;

        public int resultint;

        public Action<string> actionstring;

        public string resultstring;
    }
}