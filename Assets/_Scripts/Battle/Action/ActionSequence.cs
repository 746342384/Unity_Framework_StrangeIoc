﻿using System.Collections.Generic;

namespace _Scripts.Battle.Action
{
    public class ActionSequence : ActionBase
    {
        private int count;

        private int index;

        public List<ActionBase> list = new();

        protected override void OnInit()
        {
            index = 0;
        }

        protected override void OnUpdate()
        {
            if (index < count)
            {
                var actionBase = list[index];
                actionBase.Init();
                actionBase.Update();
                if (list[index].IsEnd) index++;
            }
            else
            {
                End();
            }
        }

        protected override void OnForceEnd()
        {
            if (index < count)
            {
                var actionBase = list[index];
                actionBase.ForceEnd();
            }
        }

        public void AddAction(ActionBase action)
        {
            list.Add(action);
            count++;
        }
    }
}