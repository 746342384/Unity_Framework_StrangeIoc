using System;
using UnityEngine;

namespace _Scripts.Battle.Action
{
    public abstract class ActionBase
    {
        public bool IsEnd
        {
            get { return mIsEnd; }
            private set { mIsEnd = value; }
        }

        public void Init()
        {
            if (mEndFrame == Time.frameCount && IsEnd)
            {
                return;
            }

            if (!isInit)
            {
                IsEnd = false;
                OnInit();
                if (!IsEnd)
                {
                    isInit = true;
                    if (ConditionBase != null && !ConditionBase())
                    {
                        End();
                    }

                    if (ConditionBase1 != null && !ConditionBase1(ConditionBase1Data))
                    {
                        End();
                    }
                }
            }
        }

        protected abstract void OnInit();

        public void Update()
        {
            if (IsEnd)
            {
                return;
            }

            if (ConditionUpdate != null && ConditionUpdate())
            {
                End();
                return;
            }

            if (ConditionContinue == null || ConditionContinue())
            {
                OnUpdate();
            }
        }

        protected virtual void OnUpdate()
        {
        }

        protected void End()
        {
            mEndFrame = Time.frameCount;
            isInit = false;
            IsEnd = true;
            OnEnd();
            OnEnd1();
        }

        public void ForceEnd()
        {
            if (IsEnd)
            {
                return;
            }

            OnForceEnd();
            End();
        }

        protected abstract void OnForceEnd();

        public void Reset()
        {
            IsEnd = false;
            isInit = false;
        }

        protected virtual void OnEnd()
        {
        }

        protected virtual void OnEnd1()
        {
        }

        public string name = string.Empty;

        public Func<bool> ConditionBase;

        public object ConditionBase1Data;

        public Func<object, bool> ConditionBase1;

        public Func<bool> ConditionUpdate;

        public Func<bool> ConditionContinue;

        private int mEndFrame;

        private bool mIsEnd;

        private bool isInit;
    }
}