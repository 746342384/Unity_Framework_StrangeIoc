using System;
using System.Collections.Generic;
using System.Linq;
using _Scripts.Framework.framework.lua.impl;
using Cysharp.Threading.Tasks;
using Framework.framework.addressable.api;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;

// 检测更新并下载资源
public class AddressableDownload : IAddressableDownload
{
    #region Download

    private AsyncOperationHandle downloadDependencies;
    private int downLoadIndex;
    private int downLoadCompleteCount;
    private readonly List<long> downLoadSizeList = new();
    private long downLoadTotalSize;
    private float curDownLoadSize;
    private DownloadInfo _downloadInfo = new();
    public event Action<DownloadInfo> OnDownloading;

    #endregion

    public IEnumerable<string> GetGroups()
    {
        var groupNamesObject = Resources.Load<GroupNameConfig>("AddressableGroupNames");
        return groupNamesObject.GroupNames;
    }


    /// <summary>
    /// 预下载
    /// </summary>
    /// <param name="key">资源包key</param>
    /// <returns></returns>
    public async UniTask StartPreDownload()
    {
        downLoadIndex = 0;
        Debug.Log("开始下载");

        // 初始化 --> 加载远端的配置文件
        await Addressables.InitializeAsync();

        var downLoadKeyList = GetGroups().ToArray();
        for (var i = 0; i < downLoadKeyList.Length; i++)
        {
            var size = Addressables.GetDownloadSizeAsync(downLoadKeyList[i]);
            Debug.Log("获取下载内容大小：" + size.Result);
            downLoadSizeList.Add(size.Result);
            downLoadTotalSize += size.Result;
        }

        if (downLoadTotalSize <= 0)
        {
            Debug.Log("无可预下载内容~");
        }
        else
        {
            for (var i = downLoadIndex; i < downLoadKeyList.Length; i++)
            {
                downloadDependencies = Addressables.DownloadDependenciesAsync(downLoadKeyList[i]);

                while (!downloadDependencies.IsDone)
                {
                    if (downloadDependencies.Status == AsyncOperationStatus.Failed)
                    {
                        downLoadIndex = i;
                        Debug.Log("下载失败，请重试...");
                        return;
                    }

                    curDownLoadSize = 0;
                    for (var j = 0; j < downLoadSizeList.Count; j++)
                    {
                        if (j < downLoadCompleteCount)
                        {
                            curDownLoadSize += downLoadSizeList[j];
                        }
                    }

                    if (downLoadCompleteCount < downLoadSizeList.Count)
                    {
                        var currentFileProgress = downloadDependencies.GetDownloadStatus().Percent;
                        var currentFileSize = downLoadSizeList[downLoadCompleteCount];
                        var currentFileDownloadSize = currentFileSize * currentFileProgress;
                        curDownLoadSize += currentFileDownloadSize;
                    }

                    var percent = curDownLoadSize * 1.0f / downLoadTotalSize;

                    if (percent <= 1)
                    {
                        _downloadInfo.CurDownLoadSize = GetFormattedFileSize(curDownLoadSize);
                        _downloadInfo.DownLoadTotalSize = GetFormattedFileSize(downLoadTotalSize);
                        _downloadInfo.Percent = curDownLoadSize / downLoadTotalSize;
                        OnDownloading?.Invoke(_downloadInfo);
                        Debug.Log($"当前文件下载大小 {_downloadInfo.CurDownLoadSize} 总下载大小 {_downloadInfo.DownLoadTotalSize}");
                    }

                    await UniTask.Delay(10);
                }

                downLoadCompleteCount = i + 1;
            }

            Debug.Log("下载完成 释放句柄");
            // 下载完成释放句柄
            Addressables.Release(downloadDependencies);
        }

        await UpdateResources();
    }

    // 将当前文件下载大小转换为以 KB 或 MB 为单位的字符串
    static string GetFormattedFileSize(float fileSize)
    {
        const float KB = 1024f;
        const float MB = 1024f * 1024f;

        switch (fileSize)
        {
            case >= MB:
            {
                var fileSizeInMB = fileSize / MB;
                return $"{fileSizeInMB:F2} MB";
            }
            case >= KB:
            {
                var fileSizeInKB = fileSize / KB;
                return $"{fileSizeInKB:F2} KB";
            }
            default:
                return $"{fileSize:F0} bytes";
        }
    }

    private async UniTask UpdateResources()
    {
        Debug.Log("检测资源是否需要更新");
        // 检查远程资源更新
        var checkHandle = Addressables.CheckForCatalogUpdates(false);
        await checkHandle;

        switch (checkHandle.Status)
        {
            case AsyncOperationStatus.None:
                Debug.Log("检测资源更新完成，没有资源需要更新");
                return;
            case AsyncOperationStatus.Failed:
                Debug.LogError("检测资源更新失败");
                return;
            case AsyncOperationStatus.Succeeded:
                Debug.Log("检测资源更新完成");
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (checkHandle.Result.Count > 0)
        {
            Debug.Log("有资源需要更新");
            var updateHandle = Addressables.UpdateCatalogs(checkHandle.Result, false);
            await updateHandle;
            var locators = updateHandle.Result;
            foreach (var locator in locators)
            {
                var keys = new List<object>();
                keys.AddRange(locator.Keys);
                var sizeHandle = Addressables.GetDownloadSizeAsync(keys);
                await sizeHandle;
                var totalDownloadSize = sizeHandle.Result;

                Debug.Log($"更新资源大小 : {GetFormattedFileSize(totalDownloadSize)}");
                if (totalDownloadSize <= 0) continue;
                var downloadHandle = Addressables.DownloadDependenciesAsync(keys);
                while (!downloadHandle.IsDone)
                {
                    var downloadedBytes = downloadHandle.GetDownloadStatus().DownloadedBytes;
                    _downloadInfo.CurDownLoadSize = GetFormattedFileSize(downloadedBytes);
                    _downloadInfo.DownLoadTotalSize = GetFormattedFileSize(totalDownloadSize);
                    _downloadInfo.Percent = downloadedBytes * 1.0f / totalDownloadSize;
                    OnDownloading?.Invoke(_downloadInfo);
                    Debug.Log(
                        $"更新文件: {downloadHandle.DebugName} 当前文件下载大小 {_downloadInfo.CurDownLoadSize} 总下载大小 {_downloadInfo.DownLoadTotalSize}");
                    await UniTask.Delay(10);
                }

                Debug.Log("下载完毕!");
                Addressables.Release(downloadHandle);
            }
        }
        else
        {
            Debug.Log("没有资源需要更新");
        }
    }
}