using System;
using System.Collections.Generic;
using _Scripts.Framework.framework.lua.impl;
using Cysharp.Threading.Tasks;

namespace Framework.framework.addressable.api
{
    public interface IAddressableDownload
    {
        event Action<DownloadInfo> OnDownloading;
        UniTask StartPreDownload();
        IEnumerable<string> GetGroups();
    }
}