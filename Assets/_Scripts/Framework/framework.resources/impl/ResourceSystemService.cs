﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Framework.framework.resources.api;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.framework.resources.impl
{
    public class ResourceSystemService : IResourceSystemService
    {
        private readonly IResourcesLoader _resourcesLoader;
        private Dictionary<string, Object> _cache = new();

        public ResourceSystemService(IResourcesLoader resourcesLoader)
        {
            _resourcesLoader = resourcesLoader;
        }

        public void Clear()
        {
            _cache.Clear();
        }

        public void Realease(GameObject gameObject)
        {
            _resourcesLoader.Realease(gameObject);
        }

        public void Realease(string key)
        {
            if (_cache.TryGetValue(key, out var value))
            {
                _resourcesLoader.Realease(value);
            }
        }

        public async Task<T> LoadAsync<T>(string path) where T : class
        {
            if (_cache.TryGetValue(path, out var obj))
            {
                return obj as T;
            }

            var fixPath = Path.Combine("Prefabs", Path.GetFileNameWithoutExtension(path));

            if (await Resources.LoadAsync<Object>(fixPath) is T resources)
            {
                return resources;
            }

            var loadAsync = await _resourcesLoader.LoadAsync<T>(path);
            return loadAsync;
        }

        public void RealeaseInstance(GameObject value)
        {
            _resourcesLoader.RealeaseInstance(value);
        }
    }
}