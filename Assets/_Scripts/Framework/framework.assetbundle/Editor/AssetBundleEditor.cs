using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace _Scripts.Framework.framework.assetbundle.Editor
{
    public class AssetBundleEditor : EditorWindow
    {
        [MenuItem("AssetBundle/AssetBundleEditor")]
        private static void ShowWindow()
        {
            var window = GetWindow<AssetBundleEditor>();
            window.titleContent = new GUIContent("AssetBundleBuild");
            window.minSize = new Vector2(500, 500);
            window.Show();
        }

        private string assetFolderPath = "ResPackage";
        private const string outPutFilePath = "RemoteResPackage/Bundles";
        private string BuildVersion;
        private string hashFilePath;
        private string manifestFilePath;

        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("资源路劲", new GUIStyle { fixedWidth = 100 });
            assetFolderPath = GUILayout.TextField(assetFolderPath);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("资源版本", new GUIStyle { fixedWidth = 100 });
            BuildVersion = GUILayout.TextField(BuildVersion);
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Build"))
            {
                BuildAllAssetFiles();
            }
        }

        private void BuildAllAssetFiles()
        {
            if (string.IsNullOrEmpty(BuildVersion)) return;
            var buildTarget = EditorUserBuildSettings.activeBuildTarget;
            var outputPath = Path.Combine(outPutFilePath, buildTarget.ToString(), BuildVersion);
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            hashFilePath = Path.Combine(outPutFilePath, buildTarget.ToString(), BuildVersion, "hash.json");

            var oldHashes = new Dictionary<string, string>();
            if (File.Exists(hashFilePath))
            {
                var json = File.ReadAllText(hashFilePath);
                oldHashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }

            var newHashes = new Dictionary<string, string>(oldHashes);
            var dataPath = Application.dataPath;
            assetFolderPath = Path.Combine(dataPath, assetFolderPath);
            var assetPaths = Directory.GetFiles(assetFolderPath, "*", SearchOption.AllDirectories);

            foreach (var assetPath in assetPaths)
            {
                if (assetPath.EndsWith(".meta")) continue;

                var hash = ComputeSha256Hash(File.ReadAllText(assetPath));
                var fixPath = assetPath.Replace(dataPath, "Assets");

                oldHashes.TryGetValue(fixPath, out var oldHash);
                if (!string.IsNullOrEmpty(oldHash) && oldHash.Equals(hash)) continue;

                var assetImporter = AssetImporter.GetAtPath(fixPath);
                assetImporter.assetBundleName = Path.GetFileNameWithoutExtension(fixPath);
                const BuildAssetBundleOptions options = BuildAssetBundleOptions.ChunkBasedCompression;
                BuildPipeline.BuildAssetBundles(outputPath, options, buildTarget);

                newHashes[fixPath] = hash;
                Debug.Log($"buildAssetBundles:{fixPath} assetBundleName:{assetImporter.assetBundleName}");
            }

            CopyToStreamingAssets(outputPath);
            DeleteOldHashFile(oldHashes, newHashes, outputPath);
            WriteHashFile(newHashes);
            WriteManifestFile();
            DeleteManifestFile(outputPath);
            AssetDatabase.Refresh();
            Debug.Log($"build done!");
        }

        private static void CopyToStreamingAssets(string outputPath)
        {
            var destFilePath = Path.Combine(Application.streamingAssetsPath, "Bundles");
            if (!Directory.Exists(destFilePath))
            {
                Directory.CreateDirectory(destFilePath);
            }

            var strings = Directory.GetFiles(outputPath);

            foreach (var s in strings)
            {
                if (s.EndsWith(".meta") || s.EndsWith(".manifest")) continue;
                var fileName = Path.GetFileName(s);
                var filePath = Path.Combine(destFilePath, fileName);
                File.Copy(s, filePath, true);
            }
        }

        private static void DeleteOldHashFile(Dictionary<string, string> oldHashes,
            IReadOnlyDictionary<string, string> newHashes, string outputPath)
        {
            foreach (var oldHash in oldHashes)
            {
                if (newHashes.ContainsKey(oldHash.Key)) continue;
                var oldAssetBundlePath = Path.Combine(outputPath, oldHash.Key);
                if (File.Exists(oldAssetBundlePath))
                {
                    File.Delete(oldAssetBundlePath);
                }

                var oldManifestPath = oldAssetBundlePath + ".manifest";
                if (File.Exists(oldManifestPath))
                {
                    File.Delete(oldManifestPath);
                }
            }
        }

        private static void DeleteManifestFile(string outputPath)
        {
            var manifestFiles = Directory.GetFiles(outputPath, "*.manifest", SearchOption.AllDirectories);
            foreach (var manifestFile in manifestFiles)
            {
                File.Delete(manifestFile);
            }
        }

        private void WriteHashFile(Dictionary<string, string> oldHashes)
        {
            if (!File.Exists(hashFilePath))
            {
                File.Create(hashFilePath).Close();
            }

            var newJson = JsonConvert.SerializeObject(oldHashes);
            File.WriteAllText(hashFilePath, newJson);
        }

        private async void WriteManifestFile()
        {
            var assetBundleManifestDict = new Dictionary<string, string[]>();
            var buildTarget = EditorUserBuildSettings.activeBuildTarget.ToString();
            var manifestPath = Path.Combine(outPutFilePath, buildTarget, BuildVersion, BuildVersion);
            var manifestBundle = AssetBundle.LoadFromFile(manifestPath);
            if (manifestBundle != null)
            {
                var manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                var assetBundles = manifest.GetAllAssetBundles();
                foreach (var assetBundle in assetBundles)
                {
                    var dependencies = manifest.GetAllDependencies(assetBundle);
                    assetBundleManifestDict[assetBundle] = dependencies;
                }

                manifestBundle.Unload(false);
            }

            manifestFilePath = Path.Combine(outPutFilePath, buildTarget, BuildVersion, "manifest.json");

            if (!File.Exists(manifestFilePath))
            {
                File.Create(manifestFilePath).Close();
            }

            var newJson = JsonConvert.SerializeObject(assetBundleManifestDict);
            await File.WriteAllTextAsync(manifestFilePath, newJson);
        }

        // 计算哈希值
        private static string ComputeSha256Hash(string rawData)
        {
            using var sha256Hash = SHA256.Create();
            var bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

            var builder = new StringBuilder();
            for (var i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }

            return builder.ToString();
        }
    }
}