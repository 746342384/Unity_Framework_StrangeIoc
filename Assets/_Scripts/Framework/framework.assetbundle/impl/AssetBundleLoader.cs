using System.Collections.Generic;
using System.IO;
using _Scripts.Framework.framework.assetbundle.config;
using UnityEngine;

namespace _Scripts.Framework.framework.assetbundle.impl
{
    public class AssetBundleLoader
    {
        private readonly Dictionary<string, AssetBundle> _loadedAssetBundles = new();
        private readonly Dictionary<string, int> _assetBundleReferenceCount = new();
        private readonly Dictionary<string, string[]> _assetBundleDependencies = new();
        private readonly AssetBundleConfig _assetBundleConfig;

        public AssetBundleLoader()
        {
            _assetBundleConfig = Resources.Load<AssetBundleConfig>(nameof(AssetBundleConfig));
        }

        // 加载AssetBundle
        public AssetBundle LoadAssetBundle(string assetBundleName)
        {
            if (_loadedAssetBundles.TryGetValue(assetBundleName, out var assetBundle))
            {
                _assetBundleReferenceCount[assetBundleName]++;
                return assetBundle;
            }

            if (_assetBundleDependencies.TryGetValue(assetBundleName, out var dependencies))
            {
                foreach (var dependency in dependencies)
                {
                    LoadAssetBundle(dependency);
                }
            }

            var assetBundlePath = _assetBundleConfig.IsEditor
                ? Path.Combine(Application.streamingAssetsPath, "Bundles", assetBundleName)
                : Path.Combine(Application.persistentDataPath, "Bundles", assetBundleName);

            if (!File.Exists(assetBundlePath))
            {
                Debug.LogError("AssetBundle file does not exist: " + assetBundlePath);
                return null;
            }

            assetBundle = AssetBundle.LoadFromFile(assetBundlePath);
            _loadedAssetBundles.Add(assetBundleName, assetBundle);
            _assetBundleReferenceCount.Add(assetBundleName, 1);
            return assetBundle;
        }

        // 卸载AssetBundle
        public void UnloadAssetBundle(string assetBundleName)
        {
            if (!_assetBundleReferenceCount.TryGetValue(assetBundleName, out var count)) return;
            count--;
            if (count <= 0)
            {
                _loadedAssetBundles[assetBundleName].Unload(false);
                _loadedAssetBundles.Remove(assetBundleName);
                _assetBundleReferenceCount.Remove(assetBundleName);
            }
            else
            {
                _assetBundleReferenceCount[assetBundleName] = count;
            }
        }
    }
}