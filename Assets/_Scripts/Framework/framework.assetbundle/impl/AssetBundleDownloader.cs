using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace _Scripts.Framework.framework.assetbundle.impl
{
    public class AssetBundleDownloader
    {
        private readonly HttpClient _httpClient;

        public AssetBundleDownloader()
        {
            _httpClient = new HttpClient();
        }

        public async UniTask<Dictionary<string, string>> DownloadHashFileAsync(string url, string savePath)
        {
            Debug.Log($"DownloadHashFileAsync:{url} to {savePath}");
            var response = await _httpClient.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                await File.WriteAllTextAsync(savePath, content);
                var hashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
                return hashes;
            }

            return null;
        }

        public async UniTask DownloadAssetBundleAsync(AssetBundleInfo info, string savePath)
        {
            Debug.Log($"DownloadAssetBundleAsync:{info.DownloadUrl} to {savePath}");
            var response = await _httpClient.GetAsync(info.DownloadUrl);
            if (response.IsSuccessStatusCode)
            {
                var bytes = await response.Content.ReadAsByteArrayAsync();
                await File.WriteAllBytesAsync(savePath + "/" + info.AssetBundleName, bytes);
            }
        }
    }
}