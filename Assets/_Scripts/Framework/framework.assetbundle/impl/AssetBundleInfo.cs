namespace _Scripts.Framework.framework.assetbundle.impl
{
    public class AssetBundleInfo
    {
        public string AssetBundleName { get; set; }
        public string DownloadUrl { get; set; }
    }
}