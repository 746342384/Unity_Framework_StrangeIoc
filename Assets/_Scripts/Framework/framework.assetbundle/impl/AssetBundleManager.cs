using System.Collections.Generic;
using System.IO;
using _Scripts.Framework.framework.assetbundle.config;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace _Scripts.Framework.framework.assetbundle.impl
{
    public class AssetBundleManager : MonoBehaviour
    {
        private string _serverUrl;
        private string _serverHashFileUrl;

        private const string hashFileName = "hash.json";
        private const string localFileDirName = "Bundles";
        private Dictionary<string, string> _serverHashes;
        private Dictionary<string, string> _localHashes;
        private readonly AssetBundleDownloader _downloader;
        private readonly AssetBundleLoader _loader;

        public AssetBundleManager()
        {
            _downloader = new AssetBundleDownloader();
            _loader = new AssetBundleLoader();
        }

        [ContextMenu("StartAsync")]
        public async UniTask StartAsync()
        {
            var assetbundleConfig =
                await Resources.LoadAsync<AssetBundleConfig>(nameof(AssetBundleConfig)) as AssetBundleConfig;
            if (assetbundleConfig == null) return;
            if (assetbundleConfig.IsEditor) return;
            _serverUrl = assetbundleConfig.ServerUrl;
            _serverHashFileUrl = _serverUrl + hashFileName;

            var localFileDir = Path.Combine(Application.persistentDataPath, localFileDirName);
            if (!Directory.Exists(localFileDir))
            {
                Directory.CreateDirectory(localFileDir);
            }

            var localHashFilePath = Path.Combine(localFileDir, hashFileName);

            if (File.Exists(localHashFilePath))
            {
                var content = await File.ReadAllTextAsync(localHashFilePath);
                _localHashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
            }
            else
            {
                _localHashes = new Dictionary<string, string>();
            }

            _serverHashes = await _downloader.DownloadHashFileAsync(_serverHashFileUrl, localHashFilePath);

            var bundlesToDownload = new List<string>();
            foreach (var pair in _serverHashes)
            {
                if (!_localHashes.TryGetValue(pair.Key, out var localHash) || localHash != pair.Value)
                {
                    bundlesToDownload.Add(pair.Key);
                }
            }

            foreach (var bundleName in bundlesToDownload)
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(bundleName);
                var info = new AssetBundleInfo
                {
                    AssetBundleName = fileNameWithoutExtension,
                    DownloadUrl = _serverUrl + fileNameWithoutExtension
                };
                await _downloader.DownloadAssetBundleAsync(info, localFileDir);
            }
        }

        public T LoadAsync<T>(string assetName) where T : Object
        {
            var loadAssetBundle = _loader.LoadAssetBundle(assetName);
            var loadAsset = loadAssetBundle.LoadAsset<T>(assetName);
            return loadAsset;
        }
    }
}