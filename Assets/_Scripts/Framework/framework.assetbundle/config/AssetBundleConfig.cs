using UnityEngine;

namespace _Scripts.Framework.framework.assetbundle.config
{
    [CreateAssetMenu(fileName = nameof(AssetBundleConfig), menuName = "Build/AssetBundleConfig", order = 0)]
    public class AssetBundleConfig : ScriptableObject
    {
        public bool IsEditor;
        public string ServerUrl;
    }
}