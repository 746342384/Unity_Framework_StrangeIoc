using System;
using _Scripts.Base.Input;
using UnityEngine.InputSystem;

namespace _Scripts.Framework.framework.input.impl
{
    public class NewInput : IInput, PlayerInput.IUIActions
    {
        private readonly PlayerInput PlayerInput;

        public NewInput()
        {
            PlayerInput = new PlayerInput();
            PlayerInput.UI.SetCallbacks(this);
            PlayerInput.Enable();
        }

        public void OnK(InputAction.CallbackContext context)
        {
        }

        public void OnEsc(InputAction.CallbackContext context)
        {
            if (!context.performed) return;
            EscAction?.Invoke();
        }

        public event Action EscAction;
    }
}