using Cysharp.Threading.Tasks;

namespace _Scripts.Framework.framework.lua.api
{
    public interface ILuaManager
    {
         UniTask CheckForUpdates();
    }
}