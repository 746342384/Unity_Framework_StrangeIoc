using UnityEngine;

namespace _Scripts.Framework.framework.lua.Config
{
    [CreateAssetMenu(fileName = "LuaBuildConfig", menuName = "Build/LuaLoadConfig", order = 0)]
    public class LuaLoadConfig : ScriptableObject
    {
        public bool IsEditor;
        public string Url;
        public string LocalUrl;
        public string LuaFilePath;
    }
}