using System.IO;
using System.Text;
using _Scripts.Framework.framework.lua.Config;
using UnityEngine;
using XLua;

namespace _Scripts.Framework.framework.lua.loader
{
    public interface IXLuaLoader
    {
        LuaTable ScriptEnv { get; }
        void Init();
        void DoString(string chunkName);
        void UnloadLuaEnv();
        void Update();
    }

    public class XLuaLoader : IXLuaLoader
    {
        private LuaEnv _luaEnv;
        private LuaLoadConfig _luaLoadConfig;

        private bool IsInited;
        private string _persistentDataPath;
        private string _localLuaFilesPath;

        public LuaTable ScriptEnv { get; private set; }

        public void DoString(string chunkName)
        {
            _luaEnv.DoString($"require '{chunkName}'", chunkName, ScriptEnv);
        }

        public void Init()
        {
            if (IsInited) return;
            _luaEnv = new LuaEnv();
            ScriptEnv = _luaEnv.NewTable();

            var meta = _luaEnv.NewTable();
            meta.Set("__index", _luaEnv.Global);
            ScriptEnv.SetMetaTable(meta);
            meta.Dispose();
            _luaEnv.AddLoader(MyLoader);

            _luaLoadConfig = Resources.Load<LuaLoadConfig>(nameof(LuaLoadConfig));
            _persistentDataPath = Application.persistentDataPath.Replace("\\", "/");
            _localLuaFilesPath = Path.Combine(_persistentDataPath, _luaLoadConfig.LuaFilePath);
        }

        private byte[] MyLoader(ref string fileName)
        {
            if (_luaLoadConfig == null) return default;
            var localLuaFilesPath = _luaLoadConfig.IsEditor ? _luaLoadConfig.LocalUrl : _localLuaFilesPath;
            var absPath = Path.Combine(localLuaFilesPath, fileName.Replace('.', '/') + ".lua");
            var readAllText = File.ReadAllText(absPath);
            var myLoader = Encoding.UTF8.GetBytes(readAllText);
            return myLoader;
        }

        public void UnloadLuaEnv()
        {
            ScriptEnv.Dispose();
            _luaEnv = null;
        }

        public void Update()
        {
            _luaEnv?.Tick();
        }
    }
}