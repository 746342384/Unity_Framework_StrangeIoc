namespace _Scripts.Framework.framework.lua.impl
{
    public class DownloadInfo
    {
        public string CurDownLoadSize;
        public string DownLoadTotalSize;
        public float Percent;
    }
}