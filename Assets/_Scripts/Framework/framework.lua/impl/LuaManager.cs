using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using _Scripts.Framework.framework.lua.api;
using _Scripts.Framework.framework.lua.Config;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

public class LuaManager : MonoBehaviour, ILuaManager
{
    private readonly HttpClient httpClient = new();

    public async UniTask CheckForUpdates()
    {
        var config = Resources.Load<LuaLoadConfig>(nameof(LuaLoadConfig));
        if (config == null)
        {
            Debug.LogError("not found lua build config");
            return;
        }

        if (config.IsEditor)
        {
            return;
        }

        var metaDataUrl = $"{config.Url}/metadata.json";
        var localLuaFilesPath = Path.Combine(Application.persistentDataPath, "Lua");
        var metadataJson = await httpClient.GetStringAsync(metaDataUrl);
        var metadata = JsonConvert.DeserializeObject<Metadata>(metadataJson);
        foreach (var entry in metadata.hashes)
        {
            var localFilePath = Path.Combine(localLuaFilesPath, entry.Key);
            if (File.Exists(localFilePath) && CalculateFileHash(localFilePath) == entry.Value) continue;
            var remoteFilePath = Path.Combine(config.Url, entry.Key);
            await DownloadFile(remoteFilePath, localFilePath);
        }
    }

    private async UniTask DownloadFile(string url, string savePath)
    {
        Debug.Log($"LuaManager DownloadFile : {url} To : {savePath}");
        var data = await httpClient.GetByteArrayAsync(url);
        Directory.CreateDirectory(Path.GetDirectoryName(savePath) ?? string.Empty);
        await File.WriteAllBytesAsync(savePath, data);
    }

    private string CalculateFileHash(string filePath)
    {
        using var stream = File.OpenRead(filePath);
        var sha = new SHA256Managed();
        var hash = sha.ComputeHash(stream);
        return GetStringFromHash(hash);
    }

    private string GetStringFromHash(byte[] hash)
    {
        var result = new StringBuilder();
        for (var i = 0; i < hash.Length; i++)
        {
            result.Append(hash[i].ToString("x2"));
        }

        return result.ToString();
    }

    private class Metadata
    {
        public int version;
        public Dictionary<string, string> hashes;
    }
}