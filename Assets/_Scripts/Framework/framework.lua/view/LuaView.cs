using System;
using _Scripts.Framework.framework.lua.loader;
using UnityEngine;

namespace _Scripts.LuaFramework.CSharp.view
{
    public class LuaView : MonoBehaviour
    {
        public string LuaName;
        private IXLuaLoader _xLuaLoader;
        private Action _luaOnDestroy;

        private void Awake()
        {
            _xLuaLoader = new XLuaLoader();
            _xLuaLoader.Init();
            _xLuaLoader.DoString(LuaName);
            _xLuaLoader.ScriptEnv.Set("self", this);
            _luaOnDestroy = _xLuaLoader.ScriptEnv.Get<Action>("onDestroy");
            var luaAwake = _xLuaLoader.ScriptEnv.Get<Action>("awake");
            luaAwake?.Invoke();
        }

        private void Start()
        {
            var luaStart = _xLuaLoader.ScriptEnv.Get<Action>("start");
            luaStart?.Invoke();
        }
        
        private void OnEnable()
        {
            var luaOnEnable = _xLuaLoader.ScriptEnv.Get<Action>("onEnable");
            luaOnEnable?.Invoke();
        }
        
        private void Update()
        {
            _xLuaLoader.Update();
            var luaUpdate = _xLuaLoader.ScriptEnv.Get<Action>("update");
            luaUpdate?.Invoke();
        }
        
        private void OnDisable()
        {
            var luaOnDisable = _xLuaLoader.ScriptEnv.Get<Action>("onDisable");
            luaOnDisable?.Invoke();
        }

        private void OnDestroy()
        {
            var luaOnDestroy = _luaOnDestroy;
            _luaOnDestroy = null;
            luaOnDestroy?.Invoke();
            _xLuaLoader.UnloadLuaEnv();
        }
    }
}