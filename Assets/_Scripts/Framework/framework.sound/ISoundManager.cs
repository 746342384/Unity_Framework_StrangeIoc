using Framework.framework.system.api;
using UnityEngine;

namespace Framework.framework.sound
{
    public interface ISoundManager : ISystem
    {
        void PlayMusic(AudioClip clip);
        void StopMusic(AudioClip clip);
        void PlaySfx(AudioClip clip, bool loop = false, float volume = 1f);
        void StopSfx(AudioClip clip);
    }
}