using System.Threading.Tasks;

namespace Framework.framework.repository
{
    public interface IRepositoryManager
    {
        Task LoadRepositories();
    }
}