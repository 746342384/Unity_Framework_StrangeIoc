namespace Module.Start
{
    public enum StartEvent
    {
        Start,
        UpdateResources,
        PreLoadResources,
        PreLoadResourcesComplete,
        UpdateLuaScriptsComplete,
        UpdateResourcesComplete,
        UpdateLuaScripts
    }
}