using Base.UI;
using Framework.framework.attribute;
using framework.framework.ui.api;
using framework.framework.ui.impl;
using Root;

namespace Module.Start.View
{
    [Act(typeof(IUIPanel), PanelNames.LoginPanel)]
    public class LoginPanel : UIPanel
    {
        public override string PanelName => PanelNames.LoginPanel;
        public override string Path => PanelPath.LoginPanel;
        public override string ContextNmae => ContextName.GameContext;
        public override UILayer Layer => UILayer.Normal;
    }
}