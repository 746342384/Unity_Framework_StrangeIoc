using Base.UI;
using framework.framework.ui.api;
using Framework.framework.ui.mediator.impl;

namespace _Scripts.Module.Start
{
    public class StartMediator : UGameMediator<StartView>
    {
        [Inject] public IPanelSystem PanelSystem { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            View.Dispatcher.AddListener(StartViewEvent.OnClickStartBtn, OnClickStartBtn);
        }

        private async void OnClickStartBtn()
        {
            await PanelSystem.OpenPanelAsync(PanelNames.UpdatePanel);
            await PanelSystem.ClosePanelAsync(PanelNames.StartPanel);
        }
    }
}