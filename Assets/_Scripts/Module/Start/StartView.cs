using Framework.framework.ui.view.impl;
using UnityEngine.UI;

namespace _Scripts.Module.Start
{
    public class StartView : UGameView
    {
        public Button StartBtn;

        protected override void Awake()
        {
            base.Awake();
            StartBtn = TransformDeepFind.FindDeepComponent<Button>(transform, nameof(StartBtn));
            StartBtn.onClick.AddListener(OnClickStart);
        }

        private void OnClickStart()
        {
            Dispatcher.Dispatch(StartViewEvent.OnClickStartBtn);
        }
    }
}