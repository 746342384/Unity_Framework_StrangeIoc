using _Scripts.Framework.framework.lua.api;
using Module.Start;
using strange.extensions.command.impl;
using UnityEngine;

namespace _Scripts.Module.Update.Command
{
    public class UpdateLuaScriptsCommand : EventCommand
    {
        [Inject] public ILuaManager LuaManager { get; set; }

        public override async void Execute()
        {
            base.Execute();
            Debug.Log("UpdateLuaScripts");
            await LuaManager.CheckForUpdates();
            Dispatcher.Dispatch(StartEvent.UpdateLuaScriptsComplete);
        }
    }
}