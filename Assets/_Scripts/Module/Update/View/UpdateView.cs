using _Scripts.Framework.framework.lua.impl;
using Framework.framework.ui.view.impl;
using TMPro;
using UnityEngine.UI;

namespace _Scripts.Module.Update.View
{
    public class UpdateView : UGameView
    {
        public TMP_Text DownloadTxt;
        public Image Progress;

        protected override void Awake()
        {
            base.Awake();
            DownloadTxt = TransformDeepFind.FindDeepComponent<TMP_Text>(transform, nameof(DownloadTxt));
            Progress = TransformDeepFind.FindDeepComponent<Image>(transform, nameof(Progress));
            DownloadTxt.text = string.Empty;
            Progress.fillAmount = 0;
        }

        public void OnDownloading(DownloadInfo downloadInfo)
        {
            DownloadTxt.text = $"{downloadInfo.CurDownLoadSize}/{downloadInfo.DownLoadTotalSize}";
            Progress.fillAmount = downloadInfo.Percent;
        }
    }
}