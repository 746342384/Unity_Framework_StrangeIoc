using System;
using _Scripts.Framework.framework.lua.impl;
using Base.UI;
using Framework.framework.addressable.api;
using framework.framework.ui.api;
using Framework.framework.ui.mediator.impl;
using Module.Start;

namespace _Scripts.Module.Update.View
{
    public class UpdateMediator : UGameMediator<UpdateView>
    {
        [Inject] public IAddressableDownload AddressableDownload { get; set; }
        [Inject] public IPanelSystem PanelSystem { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            AddressableDownload.OnDownloading += OnDownloading;
            Dispatcher.AddListener(StartEvent.UpdateResourcesComplete, UpdateResourcesComplete);
            Dispatcher.AddListener(StartEvent.UpdateLuaScriptsComplete, UpdateLuaScriptsComplete);
            Dispatcher.AddListener(StartEvent.PreLoadResourcesComplete, PreLoadResourcesComplete);
            Dispatcher.Dispatch(StartEvent.UpdateResources);
        }

        private async void PreLoadResourcesComplete()
        {
            await PanelSystem.OpenPanelAsync(PanelNames.LoginPanel);
            await PanelSystem.ClosePanelAsync(PanelNames.UpdatePanel);
        }

        private void UpdateLuaScriptsComplete()
        {
            Dispatcher.Dispatch(StartEvent.PreLoadResources);
        }

        private void UpdateResourcesComplete()
        {
            Dispatcher.Dispatch(StartEvent.UpdateLuaScripts);
        }

        private void OnDownloading(DownloadInfo downloadInfo)
        {
            View.OnDownloading(downloadInfo);
        }
    }
}