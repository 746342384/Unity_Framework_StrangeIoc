using Base.UI;
using Framework.framework.attribute;
using framework.framework.ui.api;
using framework.framework.ui.impl;
using Root;

namespace _Scripts.Module.Update.View
{
    [Act(typeof(IUIPanel),PanelNames.UpdatePanel)]
    public class UpdatePanel : UIPanel, IUIPanel
    {
        public override string PanelName => PanelNames.UpdatePanel;
        public override string Path => PanelPath.UpdatePanel;
        public override string ContextNmae  => ContextName.GameContext;
        public override UILayer Layer => UILayer.Normal;
    }
}