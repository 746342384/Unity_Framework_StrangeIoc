# 使用官方的Nginx镜像作为基础镜像
FROM nginx:latest

# 作者信息（可选）
LABEL maintainer="winjet winjetzhang@gmail.com"

# 将自定义Nginx配置文件复制到容器中
COPY nginx.conf /etc/nginx/

# 将项目文件夹复制到容器中
COPY RemoteResPackage/ /usr/share/nginx/html
