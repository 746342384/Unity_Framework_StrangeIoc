local View = require("Framework.View")
local Mediator = require("Start.StartMediator")
local Const = require("Start.StartConst")
local UnityEngine = CS.UnityEngine

StartView = View:new()

local startButton

function awake()
    Mediator:Attach(self)
    View.awake()
    startButton = UnityEngine.GameObject.Find("Button"):GetComponent(typeof(UnityEngine.UI.Button))
    startButton.onClick:AddListener(OnButtonClick)
end

function OnButtonClick()
    Mediator.eventDispatcher:dispatchEvent(Const.ClickEvent)
end

function onDestroy()
    startButton.onClick:RemoveListener(OnButtonClick)
    startButton = nil
    Mediator.Detach()
end