local Mediator = require("Framework.Mediator")
local Const = require("Start.StartConst")
StartMediator = Mediator:new()
function StartMediator:new(o)
    return Mediator.new(self, o)
end

function StartMediator:Attach(view)
    Mediator:Attach(view)
    Mediator.eventDispatcher:addEventListener(Const.ClickEvent, self.OnClick)
end

function StartMediator:OnClick()
    CS.UIManager.OpenPanel("MainPanel")
    CS.UIManager.ClosePanel("StartPanel")
end

function StartMediator:Detach()
    Mediator.eventDispatcher:removeEventListener()
    Mediator:Detach()
end

return StartMediator