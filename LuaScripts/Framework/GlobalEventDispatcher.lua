local EventDispatcher = require("Framework.EventDispatcher")

local GlobalEventDispatcher = EventDispatcher:new()

return GlobalEventDispatcher