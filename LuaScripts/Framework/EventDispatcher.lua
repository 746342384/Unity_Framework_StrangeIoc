EventDispatcher = {
    listeners = {}
}

function EventDispatcher:new()
    local instance = {
        listeners = {}
    }
    setmetatable(instance, self)
    self.__index = self
    return instance
end

function EventDispatcher:addEventListener(event, listener)
    self.listeners[event] = self.listeners[event] or {}
    table.insert(self.listeners[event], listener)
end

function EventDispatcher:dispatchEvent(event, ...)
    if self.listeners[event] then
        for _, listener in ipairs(self.listeners[event]) do
            listener(...)
        end
    end
end

function EventDispatcher:removeEventListener()
    self.listeners = nil
end
return EventDispatcher
