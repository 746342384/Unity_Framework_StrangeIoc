View = {}
View.__index = View

function View:new()
    local self = setmetatable({}, self)
    return self
end

function View:awake()
end

function View:start()
end

function View:onEnable()
end

function View:update()
end

function View:onDisable()
end

function View:onDestroy()
end

return View
