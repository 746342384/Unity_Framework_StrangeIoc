local EventDispatcher = require("Framework.GlobalEventDispatcher") 

Mediator = {
    eventDispatcher = EventDispatcher
}

function Mediator:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.view = nil
    return o
end

function Mediator:Attach(view)
    self.view = view
end

function Mediator:Detach()
    self.eventDispatcher = nil
    self.view = nil
end

return Mediator

