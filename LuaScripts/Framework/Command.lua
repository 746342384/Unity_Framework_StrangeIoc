Command = {}

function Command:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function Command:execute()
    -- 在具体的命令中重写这个方法
end